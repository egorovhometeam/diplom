\c galaxy
\echo Next connection info string
\conninfo
\echo Set client encoding to WIN1251:
\encoding WIN1251

TRUNCATE t_contacts RESTART IDENTITY CASCADE;
TRUNCATE t_companies RESTART IDENTITY CASCADE;
TRUNCATE t_contacttype RESTART IDENTITY CASCADE;
TRUNCATE t_currencies RESTART IDENTITY CASCADE;
TRUNCATE t_group2company RESTART IDENTITY CASCADE;
TRUNCATE t_group2material RESTART IDENTITY CASCADE;
TRUNCATE t_group2measuretype RESTART IDENTITY CASCADE;
TRUNCATE t_group2model RESTART IDENTITY CASCADE;
TRUNCATE t_group2modelparam RESTART IDENTITY CASCADE;
TRUNCATE t_group2namegenerator RESTART IDENTITY CASCADE;
TRUNCATE t_group2operation RESTART IDENTITY CASCADE;
TRUNCATE t_group2serial RESTART IDENTITY CASCADE;
TRUNCATE t_group2state RESTART IDENTITY CASCADE;
TRUNCATE t_groups RESTART IDENTITY CASCADE;
TRUNCATE t_measures RESTART IDENTITY CASCADE;
TRUNCATE t_measuretypes RESTART IDENTITY CASCADE;
TRUNCATE t_modelitems RESTART IDENTITY CASCADE;
TRUNCATE t_modelparams RESTART IDENTITY CASCADE;
TRUNCATE t_models RESTART IDENTITY CASCADE;
TRUNCATE t_namegenerator RESTART IDENTITY CASCADE;
TRUNCATE t_operationitems RESTART IDENTITY CASCADE;
TRUNCATE t_operationitemtypes RESTART IDENTITY CASCADE;
TRUNCATE t_operations RESTART IDENTITY CASCADE;
TRUNCATE t_ordercalcs RESTART IDENTITY CASCADE;
TRUNCATE t_orderitems RESTART IDENTITY CASCADE;
TRUNCATE t_orderitemtypes RESTART IDENTITY CASCADE;
TRUNCATE t_orders RESTART IDENTITY CASCADE;
TRUNCATE t_people2role RESTART IDENTITY CASCADE;
TRUNCATE t_peoples RESTART IDENTITY CASCADE;
TRUNCATE t_prepacks RESTART IDENTITY CASCADE;
TRUNCATE t_prices RESTART IDENTITY CASCADE;
TRUNCATE t_roles RESTART IDENTITY CASCADE;
TRUNCATE t_staff RESTART IDENTITY CASCADE;
TRUNCATE t_states RESTART IDENTITY CASCADE;

INSERT INTO t_groups (f_id,f_name,f_comments) VALUES 
(1,'Заказы','')
,(2,'Физлица','')
,(3,'Измерения баннеров','')
,(4,'Измерения печати на баннерах','')
,(5,'Материалы для баннеров','')
,(6,'Измерения расходных материалов для печати','')
,(7,'Измерения трудозатрат','')
,(8,'Выбор материала','')
,(9,'Выбор операций для банеров прямоугольных','')
,(10,'Измерение площади','')
,(11,'Операции для баннеров','')
;
INSERT INTO t_states (f_id,f_name) VALUES 
(1,'новый'),(2,'забронирован'),(3,'допущен к производству'),(4,'в производстве'),(5,'на складе'),(6,'отгружен')
,(7,'активен'),(8,'заблокирован');
INSERT INTO t_group2state (f_idgroup,f_idstate) VALUES (1,1),(1,2),(1,3),(1,4),(1,5),(1,6),(2,7),(2,8);

INSERT INTO t_peoples (f_id,f_idtenant,f_login,f_password,f_firstname,f_middlename,f_lastname,f_birthday,f_dtcre,f_idcreator,f_idstate,f_comments) VALUES 
(1,1,'admin','admin','Админ','Админович','Админов','1970-03-15',now(),1,1,'')
,(2,1,'guest','guest','Гость','Гостевич','Гостев','1969-12-08',now(),1,1,'')
,(3,1,'user','user','Юзер','Юзерович','Юзеров','1980-05-12',now(),1,1,'')
,(4,1,'chief','chief','Шеф','Шефович','Шефов','1959-04-18',now(),1,1,'')
,(5,1,'manager','manager','Манагер','Манагерович','Манагеров','1981-09-10',now(),1,1,'')
,(6,1,'production','production','Производство','Производствович','Производствов','1979-02-28',now(),1,1,'')
,(7,1,'worker','worker','Рабочий','Работникович','Работников','1975-06-02',now(),1,1,'')
,(8,1,'ivan','ivan','Иван','Кириллович','Старшинов','1970-03-15',now(),1,1,'')
,(9,1,'marina','marina','Марина','Семеновна','Гладких','1965-10-05',now(),1,1,'')
,(10,1,'galina','galina','Галина','Николаевна','Лапшина','1985-07-02',now(),1,1,'')
,(11,1,'victor','victor','Виктор','Михайлович','Кириллов','1974-03-22',now(),1,1,'')
,(12,1,'leon','leon','Леон','Леонович','Леонов','1970-09-21',now(),1,1,'')
;

INSERT INTO t_roles VALUES (1,'admin_role'),(2,'guest_role'),(3,'user_role'),(4,'chief_role'),(5,'manager_role')
,(6,'production_role'),(7,'worker_role');

INSERT INTO t_people2role VALUES (1,1),(2,2),(3,3),(4,4),(5,5),(6,6),(7,7)
,(8,4),(8,7)
,(9,5),(9,7)
,(10,3)
,(11,7)
,(12,1),(12,5),(12,6),(12,7);

INSERT INTO t_measuretypes VALUES 
(1,'Количество')
,(2,'Длина')
,(3,'Площадь')
,(4,'Объем')
,(5,'Вес')
,(6,'Время')
,(7,'Скорость')
,(8,'Разрешение')
,(9,'Идентификатор');
INSERT INTO t_measures (f_id,f_idmeasuretype,f_name) VALUES 
(1,1,'шт')
,(2,2,'м'),(3,2,'см'),(4,2,'мм')
,(5,3,'кв.м'),(6,3,'кв.см'),(7,3,'кв.мм')
,(8,4,'куб.м'),(9,4,'куб.см'),(10,4,'куб.мм'),(11,4,'л')
,(12,5,'т'),(13,5,'кг'),(14,5,'гр')
,(15,6,'месяц'),(16,6,'неделя'),(17,6,'день'),(18,6,'час'),(19,6,'мин'),(20,6,'сек')
,(21,7,'км/ч'),(22,7,'м/ч'),(23,7,'м/мин'),(24,7,'м/сек'),(25,7,'см/ч'),(26,7,'см/мин')
,(27,8,'dpi')
;

INSERT INTO t_group2measuretype (f_idgroup,f_idmeasuretype) VALUES (3,3),(4,8),(6,5),(7,6),(8,9),(9,9),(10,3);

INSERT INTO t_companies (f_id,f_name,f_shortname,f_comments) VALUES 
(1,'ООО "Рога и Копыта"','ООО "Рога и Копыта"',''),(2,'ИП Вертикаль','ИП Вертикаль',''),(3,'ППГХО','ППГХО','')
,(4,'Поставщик тонера для широкоформатного принтера','Поставщик тонера','');

INSERT INTO t_materials (f_id,f_marking,f_name,f_description,f_idvendor,f_idmeasuregroup,f_comments) VALUES 
(1,'Пленка Китай','Пленка Китай','',1,1,''),(2,'Китай 440','Китай 440','',1,1,'')
,(3,'Цв. тонер для широкоформатного принтера','Цв. тонер для широкоформатного принтера','',4,6,'');
INSERT INTO t_group2material VALUES (5,1),(5,2);

INSERT INTO t_operationitemtypes (f_id,f_name) VALUES (1,'Расходные материалы'),(2,'Трудозатраты');

INSERT INTO t_operations (f_id,f_marking,f_name,f_description,f_comments) VALUES 
(1,'Печать на широкоформатном принтере','Печать на широкоформатном принтере','','')
;
INSERT INTO t_operationitems (f_id,f_idoperation,f_idoperationitemtype,f_idofsome,f_idmeasuregroup) VALUES 
(1,1,1,3,6),(2,1,2,NULL,7);
INSERT INTO t_group2operation (f_idgroup,f_idoperation) VALUES (11,1);
--INSERT INTO orderitemtypes (id,name) VALUES (1,'Материалы'),(2,'Операции'),(3,'Полуфабрикаты');
INSERT INTO t_orderitemtypes (f_id,f_name) VALUES (1,'Продукция'),(2,'Материалы'),(3,'Услуги');

INSERT INTO t_models (f_id,f_idorderitemtype,f_name,f_comments) VALUES 
(1,2,'Материалы','')
,(2,3,'Доставка','')
,(3,3,'Монтаж','')
,(4,1,'Баннер Прямоугольный Простой',''),(5,1,'Баннер Прямоугольный с Крепежом','')
;
INSERT INTO t_modelparams (f_id,f_name,f_idmeasuregroup) VALUES 
(1,'Материал',8),(2,'Площадь прямоугольника',10),(3,'Операция',9);
INSERT INTO t_modelparamitems (f_id,f_idmodelparam,f_name,f_idmeasure) VALUES 
(1,2,'Длина',4),(2,2,'Ширина',4)
;
INSERT INTO t_modelitems (f_id,f_name,f_comments,f_idmodel,f_idmodelparam) VALUES
(1,'Материал для баннера','',4,1),(2,'Площадь','',4,2),(3,'Печать для баннера','',4,3)
,(4,'Артикул материала','',1,1)
;

INSERT INTO t_currencies (f_id,f_name,f_shortname,f_abbreviation) VALUES (1,'Российский рубль','руб','RUB'),(2,'Доллар США','долл','USD'),(3,'Евро','евро','EUR');

INSERT INTO t_prices (f_id,f_idmaterial,f_idbasemeasure,f_idcurrency,f_price) VALUES (1,1,5,1,550),(2,2,5,1,450),(3,3,14,1,20);

--######################################################### DATA ENTRY #################################################################################################
INSERT INTO t_orders (f_id,f_idtenant,f_idcontragent,f_idworker,f_dtcre,f_name,f_sum,f_sumtax,f_dtout,f_discount,f_idstate,f_comments) VALUES 
(1,1,1,9,'2012-03-15 16:07:22','РГ-1',3050.60,350.12,'2012-03-20',0.0,1,'Первый заказ!!!')
,(2,1,2,5,'2012-03-15 17:17:20','ВЕРТ-1',1050.50,120.12,'2012-03-20',0.0,1,'Второй заказ!!!')
,(3,1,3,12,'2012-03-16 10:17:20','ППГ-1',10050.50,1200.12,'2012-03-21',0.0,1,'Третий заказ!!!')
;
INSERT INTO t_orderitems (f_id,f_idorder,f_idmodel,f_numpos,f_nameitem,f_amount,f_sum,f_sumtax,f_comments) VALUES 
(1,1,4,1,'Баннер Прямоугольный Простой',1,100.50,10.05,''),(2,1,1,2,'Пленка Китай',1,50.25,5.06,'')
,(3,2,4,1,'Баннер Прямоугольный Простой',2,3050.64,305,'')
,(4,3,4,1,'Баннер Прямоугольный Простой',1,2050.43,205.4,''),(5,3,1,2,'Пленка Китай',3,60.0,0.6,''),(6,3,4,3,'Баннер Прямоугольный Простой',2,100.0,10.0,'');
INSERT INTO t_ordercalcs (f_id,f_idorderitem,f_idmodel,f_idorderitemtype,f_idmodelparam,f_idparam,f_idmeasure,f_price,f_value,f_sum,f_sumtax) VALUES 
(1,1,4,1,1,1,5,550,2,1100,110);

--INSERT INTO workprices VALUES (1,1,1,1,'720',NULL),(2,1,1,2,'Пленка Китай',190.0),(3,1,2,1,'360',120.0),(4,1,2,2,'Китай 440',NULL);
--INSERT INTO workpriceparams VALUES (1,'Плотность'),(2,'Материал');

--INSERT INTO groupserials (idgroupserial,name) VALUES (1,'orders_contragents');
--INSERT INTO sernumbers (id,idgroupserial,newserial) VALUES (1,1,2),(2,1,2),(3,1,2);

--INSERT INTO groupgenerators (idgroupgenerator,name) VALUES (1,'contragents_orders');
--INSERT INTO namegenerator (idgroupgenerator,id,prefix,middle,suffix) VALUES (1,1,'РК-','',''),(1,2,'ВЕРТ-','',''),(1,3,'ППГ-','','');