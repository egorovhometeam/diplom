\c crmdb

ALTER TABLE users DROP CONSTRAINT IF EXISTS fk_users_departments;
ALTER TABLE users ADD CONSTRAINT fk_users_departments FOREIGN KEY (id_department) REFERENCES departments;

ALTER TABLE users DROP CONSTRAINT IF EXISTS fk_users_roles;
ALTER TABLE users ADD CONSTRAINT fk_users_roles FOREIGN KEY (id_role) REFERENCES roles;
