\echo Next connection info string
\conninfo

-- Database: crmdb

DROP DATABASE IF EXISTS crmdb;

-- create user crmadmin
drop user if exists crmadmin;
create user crmadmin with password 'crmadminpwd';

CREATE DATABASE crmdb
  WITH OWNER = crmadmin
       TEMPLATE = template0
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'ru_RU.utf8'
       LC_CTYPE = 'ru_RU.utf8'
       CONNECTION LIMIT = -1;

-- Next command not needed because crmadmin is own of database
--grant all privileges on database crmdb to crmadmin;

\c crmdb

-- Table: test


  
--DROP TABLE IF EXISTS departments;

CREATE TABLE IF NOT EXISTS departments
(
  id SERIAL NOT NULL,
  name VARCHAR(20),
  CONSTRAINT departments_pkey PRIMARY KEY(id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE departments
  OWNER TO crmadmin;

--DROP TABLE IF EXISTS users;

CREATE TABLE IF NOT EXISTS users
(
  id SERIAL  NOT NULL,
  id_department INTEGER   NOT NULL,
  id_role INTEGER   NOT NULL,
  short_name VARCHAR(20),
  full_name VARCHAR(30),
  pwd VARCHAR(10),
  beginDate DATE,
  birthDate DATE,
  phone VARCHAR(15),
  email VARCHAR(30),
  CONSTRAINT users_pkey PRIMARY KEY(id) 
)
WITH (
  OIDS=FALSE
);
ALTER TABLE users
  OWNER TO crmadmin;

--DROP TABLE IF EXISTS roles;

CREATE TABLE IF NOT EXISTS roles
(
  id SERIAL  NOT NULL,
  name VARCHAR(20),
  access INTEGER,
  CONSTRAINT roles_pkey PRIMARY KEY(id) 
)
WITH (
  OIDS=FALSE
);
ALTER TABLE roles
  OWNER TO crmadmin;

--##############################################
