--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

ALTER TABLE ONLY public.welcome_post DROP CONSTRAINT welcome_post_author_id_3a3f7e4b_fk_auth_user_id;
ALTER TABLE ONLY public.polls_choice DROP CONSTRAINT polls_choice_question_id_c5b4b260_fk_polls_question_id;
ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id;
ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co;
ALTER TABLE ONLY public.crm_visit DROP CONSTRAINT crm_visit_id_event_id_8db3837f_fk_crm_event_id;
ALTER TABLE ONLY public.crm_visit DROP CONSTRAINT crm_visit_id_client_id_2056817e_fk_crm_client_id;
ALTER TABLE ONLY public.crm_client DROP CONSTRAINT "crm_client_id_role_id_4e3999ca_fk_clManager_role_id";
ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id;
ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm;
ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id;
ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id;
ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co;
ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id;
ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm;
DROP INDEX public.welcome_post_author_id_3a3f7e4b;
DROP INDEX public.polls_choice_question_id_c5b4b260;
DROP INDEX public.django_session_session_key_c0390e0f_like;
DROP INDEX public.django_session_expire_date_a5c62663;
DROP INDEX public.django_admin_log_user_id_c564eba6;
DROP INDEX public.django_admin_log_content_type_id_c4bce8eb;
DROP INDEX public.crm_visit_id_event_id_8db3837f;
DROP INDEX public.crm_visit_id_client_id_2056817e;
DROP INDEX public.crm_client_id_role_id_4e3999ca;
DROP INDEX public.auth_user_username_6821ab7c_like;
DROP INDEX public.auth_user_user_permissions_user_id_a95ead1b;
DROP INDEX public.auth_user_user_permissions_permission_id_1fbb5f2c;
DROP INDEX public.auth_user_groups_user_id_6a12ed8b;
DROP INDEX public.auth_user_groups_group_id_97559544;
DROP INDEX public.auth_permission_content_type_id_2f476e4b;
DROP INDEX public.auth_group_permissions_permission_id_84c5c92e;
DROP INDEX public.auth_group_permissions_group_id_b120cbf9;
DROP INDEX public.auth_group_name_a6ea08ec_like;
ALTER TABLE ONLY public.welcome_post DROP CONSTRAINT welcome_post_pkey;
ALTER TABLE ONLY public.client DROP CONSTRAINT users_pkey;
ALTER TABLE ONLY public.roles DROP CONSTRAINT roles_pkey;
ALTER TABLE ONLY public.polls_question DROP CONSTRAINT polls_question_pkey;
ALTER TABLE ONLY public.polls_choice DROP CONSTRAINT polls_choice_pkey;
ALTER TABLE ONLY public.django_session DROP CONSTRAINT django_session_pkey;
ALTER TABLE ONLY public.django_migrations DROP CONSTRAINT django_migrations_pkey;
ALTER TABLE ONLY public.django_content_type DROP CONSTRAINT django_content_type_pkey;
ALTER TABLE ONLY public.django_content_type DROP CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq;
ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_pkey;
ALTER TABLE ONLY public.crm_visit DROP CONSTRAINT crm_visit_pkey;
ALTER TABLE ONLY public.crm_event DROP CONSTRAINT crm_event_pkey;
ALTER TABLE ONLY public.crm_client DROP CONSTRAINT crm_client_pkey;
ALTER TABLE ONLY public.crm_role DROP CONSTRAINT "clManager_role_pkey";
ALTER TABLE ONLY public.auth_user DROP CONSTRAINT auth_user_username_key;
ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq;
ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_pkey;
ALTER TABLE ONLY public.auth_user DROP CONSTRAINT auth_user_pkey;
ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq;
ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_pkey;
ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_pkey;
ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq;
ALTER TABLE ONLY public.auth_group DROP CONSTRAINT auth_group_pkey;
ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_pkey;
ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq;
ALTER TABLE ONLY public.auth_group DROP CONSTRAINT auth_group_name_key;
ALTER TABLE public.welcome_post ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.roles ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.polls_question ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.polls_choice ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.django_migrations ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.django_content_type ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.django_admin_log ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.crm_visit ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.crm_role ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.crm_event ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.crm_client ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.client ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_user_user_permissions ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_user_groups ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_user ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_permission ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_group_permissions ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_group ALTER COLUMN id DROP DEFAULT;
DROP SEQUENCE public.welcome_post_id_seq;
DROP TABLE public.welcome_post;
DROP SEQUENCE public.users_id_seq;
DROP SEQUENCE public.roles_id_seq;
DROP TABLE public.roles;
DROP SEQUENCE public.polls_question_id_seq;
DROP TABLE public.polls_question;
DROP SEQUENCE public.polls_choice_id_seq;
DROP TABLE public.polls_choice;
DROP TABLE public.django_session;
DROP SEQUENCE public.django_migrations_id_seq;
DROP TABLE public.django_migrations;
DROP SEQUENCE public.django_content_type_id_seq;
DROP TABLE public.django_content_type;
DROP SEQUENCE public.django_admin_log_id_seq;
DROP TABLE public.django_admin_log;
DROP SEQUENCE public.crm_visit_id_seq;
DROP TABLE public.crm_visit;
DROP SEQUENCE public.crm_event_id_seq;
DROP TABLE public.crm_event;
DROP SEQUENCE public.crm_client_id_seq;
DROP TABLE public.crm_client;
DROP TABLE public.client;
DROP SEQUENCE public."clManager_role_id_seq";
DROP TABLE public.crm_role;
DROP SEQUENCE public.auth_user_user_permissions_id_seq;
DROP TABLE public.auth_user_user_permissions;
DROP SEQUENCE public.auth_user_id_seq;
DROP SEQUENCE public.auth_user_groups_id_seq;
DROP TABLE public.auth_user_groups;
DROP TABLE public.auth_user;
DROP SEQUENCE public.auth_permission_id_seq;
DROP TABLE public.auth_permission;
DROP SEQUENCE public.auth_group_permissions_id_seq;
DROP TABLE public.auth_group_permissions;
DROP SEQUENCE public.auth_group_id_seq;
DROP TABLE public.auth_group;
DROP EXTENSION plpgsql;
DROP SCHEMA public;
--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE auth_group OWNER TO crmadmin;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: crmadmin
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_id_seq OWNER TO crmadmin;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: crmadmin
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_group_permissions OWNER TO crmadmin;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: crmadmin
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_permissions_id_seq OWNER TO crmadmin;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: crmadmin
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE auth_permission OWNER TO crmadmin;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: crmadmin
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_permission_id_seq OWNER TO crmadmin;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: crmadmin
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE auth_user OWNER TO crmadmin;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE auth_user_groups OWNER TO crmadmin;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: crmadmin
--

CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_groups_id_seq OWNER TO crmadmin;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: crmadmin
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: crmadmin
--

CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_id_seq OWNER TO crmadmin;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: crmadmin
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_user_user_permissions OWNER TO crmadmin;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: crmadmin
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_user_permissions_id_seq OWNER TO crmadmin;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: crmadmin
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- Name: crm_role; Type: TABLE; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE TABLE crm_role (
    id integer NOT NULL,
    name character varying(20),
    access integer
);


ALTER TABLE crm_role OWNER TO crmadmin;

--
-- Name: clManager_role_id_seq; Type: SEQUENCE; Schema: public; Owner: crmadmin
--

CREATE SEQUENCE "clManager_role_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "clManager_role_id_seq" OWNER TO crmadmin;

--
-- Name: clManager_role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: crmadmin
--

ALTER SEQUENCE "clManager_role_id_seq" OWNED BY crm_role.id;


--
-- Name: client; Type: TABLE; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE TABLE client (
    id integer NOT NULL,
    id_role integer NOT NULL,
    login character varying(20),
    full_name character varying(30),
    pwd character varying(10),
    begindate date,
    birthdate date,
    phone character varying(15),
    email character varying(30)
);


ALTER TABLE client OWNER TO crmadmin;

--
-- Name: crm_client; Type: TABLE; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE TABLE crm_client (
    id integer NOT NULL,
    login character varying(20),
    full_name character varying(30),
    pwd character varying(10),
    begindate date,
    birthdate date,
    phone character varying(15),
    email character varying(254),
    site character varying(50),
    id_role_id integer NOT NULL
);


ALTER TABLE crm_client OWNER TO crmadmin;

--
-- Name: crm_client_id_seq; Type: SEQUENCE; Schema: public; Owner: crmadmin
--

CREATE SEQUENCE crm_client_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE crm_client_id_seq OWNER TO crmadmin;

--
-- Name: crm_client_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: crmadmin
--

ALTER SEQUENCE crm_client_id_seq OWNED BY crm_client.id;


--
-- Name: crm_event; Type: TABLE; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE TABLE crm_event (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    description text NOT NULL,
    fee double precision,
    notes text
);


ALTER TABLE crm_event OWNER TO crmadmin;

--
-- Name: crm_event_id_seq; Type: SEQUENCE; Schema: public; Owner: crmadmin
--

CREATE SEQUENCE crm_event_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE crm_event_id_seq OWNER TO crmadmin;

--
-- Name: crm_event_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: crmadmin
--

ALTER SEQUENCE crm_event_id_seq OWNED BY crm_event.id;


--
-- Name: crm_visit; Type: TABLE; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE TABLE crm_visit (
    id integer NOT NULL,
    date date,
    note character varying(50),
    id_client_id integer NOT NULL,
    id_event_id integer NOT NULL
);


ALTER TABLE crm_visit OWNER TO crmadmin;

--
-- Name: crm_visit_id_seq; Type: SEQUENCE; Schema: public; Owner: crmadmin
--

CREATE SEQUENCE crm_visit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE crm_visit_id_seq OWNER TO crmadmin;

--
-- Name: crm_visit_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: crmadmin
--

ALTER SEQUENCE crm_visit_id_seq OWNED BY crm_visit.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE django_admin_log OWNER TO crmadmin;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: crmadmin
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_admin_log_id_seq OWNER TO crmadmin;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: crmadmin
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE django_content_type OWNER TO crmadmin;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: crmadmin
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_content_type_id_seq OWNER TO crmadmin;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: crmadmin
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE django_migrations OWNER TO crmadmin;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: crmadmin
--

CREATE SEQUENCE django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_migrations_id_seq OWNER TO crmadmin;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: crmadmin
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE django_session OWNER TO crmadmin;

--
-- Name: polls_choice; Type: TABLE; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE TABLE polls_choice (
    id integer NOT NULL,
    choice_text character varying(200) NOT NULL,
    votes integer NOT NULL,
    question_id integer NOT NULL
);


ALTER TABLE polls_choice OWNER TO crmadmin;

--
-- Name: polls_choice_id_seq; Type: SEQUENCE; Schema: public; Owner: crmadmin
--

CREATE SEQUENCE polls_choice_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE polls_choice_id_seq OWNER TO crmadmin;

--
-- Name: polls_choice_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: crmadmin
--

ALTER SEQUENCE polls_choice_id_seq OWNED BY polls_choice.id;


--
-- Name: polls_question; Type: TABLE; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE TABLE polls_question (
    id integer NOT NULL,
    question_text character varying(200) NOT NULL,
    pub_date timestamp with time zone NOT NULL
);


ALTER TABLE polls_question OWNER TO crmadmin;

--
-- Name: polls_question_id_seq; Type: SEQUENCE; Schema: public; Owner: crmadmin
--

CREATE SEQUENCE polls_question_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE polls_question_id_seq OWNER TO crmadmin;

--
-- Name: polls_question_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: crmadmin
--

ALTER SEQUENCE polls_question_id_seq OWNED BY polls_question.id;


--
-- Name: roles; Type: TABLE; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE TABLE roles (
    id integer NOT NULL,
    name character varying(20),
    access integer
);


ALTER TABLE roles OWNER TO crmadmin;

--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: crmadmin
--

CREATE SEQUENCE roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE roles_id_seq OWNER TO crmadmin;

--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: crmadmin
--

ALTER SEQUENCE roles_id_seq OWNED BY roles.id;


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: crmadmin
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO crmadmin;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: crmadmin
--

ALTER SEQUENCE users_id_seq OWNED BY client.id;


--
-- Name: welcome_post; Type: TABLE; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE TABLE welcome_post (
    id integer NOT NULL,
    title character varying(200) NOT NULL,
    text text NOT NULL,
    created_date timestamp with time zone NOT NULL,
    published_date timestamp with time zone,
    author_id integer NOT NULL
);


ALTER TABLE welcome_post OWNER TO crmadmin;

--
-- Name: welcome_post_id_seq; Type: SEQUENCE; Schema: public; Owner: crmadmin
--

CREATE SEQUENCE welcome_post_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE welcome_post_id_seq OWNER TO crmadmin;

--
-- Name: welcome_post_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: crmadmin
--

ALTER SEQUENCE welcome_post_id_seq OWNED BY welcome_post.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: crmadmin
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: crmadmin
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: crmadmin
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: crmadmin
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: crmadmin
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: crmadmin
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: crmadmin
--

ALTER TABLE ONLY client ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: crmadmin
--

ALTER TABLE ONLY crm_client ALTER COLUMN id SET DEFAULT nextval('crm_client_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: crmadmin
--

ALTER TABLE ONLY crm_event ALTER COLUMN id SET DEFAULT nextval('crm_event_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: crmadmin
--

ALTER TABLE ONLY crm_role ALTER COLUMN id SET DEFAULT nextval('"clManager_role_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: crmadmin
--

ALTER TABLE ONLY crm_visit ALTER COLUMN id SET DEFAULT nextval('crm_visit_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: crmadmin
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: crmadmin
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: crmadmin
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: crmadmin
--

ALTER TABLE ONLY polls_choice ALTER COLUMN id SET DEFAULT nextval('polls_choice_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: crmadmin
--

ALTER TABLE ONLY polls_question ALTER COLUMN id SET DEFAULT nextval('polls_question_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: crmadmin
--

ALTER TABLE ONLY roles ALTER COLUMN id SET DEFAULT nextval('roles_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: crmadmin
--

ALTER TABLE ONLY welcome_post ALTER COLUMN id SET DEFAULT nextval('welcome_post_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: crmadmin
--



--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: crmadmin
--

SELECT pg_catalog.setval('auth_group_id_seq', 1, false);


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: crmadmin
--



--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: crmadmin
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 1, false);


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: crmadmin
--

INSERT INTO auth_permission VALUES (1, 'Can add log entry', 1, 'add_logentry');
INSERT INTO auth_permission VALUES (2, 'Can change log entry', 1, 'change_logentry');
INSERT INTO auth_permission VALUES (3, 'Can delete log entry', 1, 'delete_logentry');
INSERT INTO auth_permission VALUES (4, 'Can add group', 2, 'add_group');
INSERT INTO auth_permission VALUES (5, 'Can change group', 2, 'change_group');
INSERT INTO auth_permission VALUES (6, 'Can delete group', 2, 'delete_group');
INSERT INTO auth_permission VALUES (7, 'Can add permission', 3, 'add_permission');
INSERT INTO auth_permission VALUES (8, 'Can change permission', 3, 'change_permission');
INSERT INTO auth_permission VALUES (9, 'Can delete permission', 3, 'delete_permission');
INSERT INTO auth_permission VALUES (10, 'Can add user', 4, 'add_user');
INSERT INTO auth_permission VALUES (11, 'Can change user', 4, 'change_user');
INSERT INTO auth_permission VALUES (12, 'Can delete user', 4, 'delete_user');
INSERT INTO auth_permission VALUES (13, 'Can add content type', 5, 'add_contenttype');
INSERT INTO auth_permission VALUES (14, 'Can change content type', 5, 'change_contenttype');
INSERT INTO auth_permission VALUES (15, 'Can delete content type', 5, 'delete_contenttype');
INSERT INTO auth_permission VALUES (16, 'Can add session', 6, 'add_session');
INSERT INTO auth_permission VALUES (17, 'Can change session', 6, 'change_session');
INSERT INTO auth_permission VALUES (18, 'Can delete session', 6, 'delete_session');
INSERT INTO auth_permission VALUES (19, 'Can add choice', 7, 'add_choice');
INSERT INTO auth_permission VALUES (20, 'Can change choice', 7, 'change_choice');
INSERT INTO auth_permission VALUES (21, 'Can delete choice', 7, 'delete_choice');
INSERT INTO auth_permission VALUES (22, 'Can add question', 8, 'add_question');
INSERT INTO auth_permission VALUES (23, 'Can change question', 8, 'change_question');
INSERT INTO auth_permission VALUES (24, 'Can delete question', 8, 'delete_question');
INSERT INTO auth_permission VALUES (25, 'Can add post', 9, 'add_post');
INSERT INTO auth_permission VALUES (26, 'Can change post', 9, 'change_post');
INSERT INTO auth_permission VALUES (27, 'Can delete post', 9, 'delete_post');
INSERT INTO auth_permission VALUES (28, 'Can add clients', 10, 'add_clients');
INSERT INTO auth_permission VALUES (29, 'Can change clients', 10, 'change_clients');
INSERT INTO auth_permission VALUES (30, 'Can delete clients', 10, 'delete_clients');
INSERT INTO auth_permission VALUES (31, 'Can add roles', 11, 'add_roles');
INSERT INTO auth_permission VALUES (32, 'Can change roles', 11, 'change_roles');
INSERT INTO auth_permission VALUES (33, 'Can delete roles', 11, 'delete_roles');
INSERT INTO auth_permission VALUES (34, 'Can add visits', 12, 'add_visits');
INSERT INTO auth_permission VALUES (35, 'Can change visits', 12, 'change_visits');
INSERT INTO auth_permission VALUES (36, 'Can delete visits', 12, 'delete_visits');
INSERT INTO auth_permission VALUES (37, 'Can add events happened', 13, 'add_eventshappened');
INSERT INTO auth_permission VALUES (38, 'Can change events happened', 13, 'change_eventshappened');
INSERT INTO auth_permission VALUES (39, 'Can delete events happened', 13, 'delete_eventshappened');
INSERT INTO auth_permission VALUES (40, 'Can add events', 14, 'add_events');
INSERT INTO auth_permission VALUES (41, 'Can change events', 14, 'change_events');
INSERT INTO auth_permission VALUES (42, 'Can delete events', 14, 'delete_events');
INSERT INTO auth_permission VALUES (43, 'Can add role', 15, 'add_role');
INSERT INTO auth_permission VALUES (44, 'Can change role', 15, 'change_role');
INSERT INTO auth_permission VALUES (45, 'Can delete role', 15, 'delete_role');
INSERT INTO auth_permission VALUES (46, 'Can add client', 16, 'add_client');
INSERT INTO auth_permission VALUES (47, 'Can change client', 16, 'change_client');
INSERT INTO auth_permission VALUES (48, 'Can delete client', 16, 'delete_client');
INSERT INTO auth_permission VALUES (49, 'Can add event', 17, 'add_event');
INSERT INTO auth_permission VALUES (50, 'Can change event', 17, 'change_event');
INSERT INTO auth_permission VALUES (51, 'Can delete event', 17, 'delete_event');
INSERT INTO auth_permission VALUES (52, 'Can add event happened', 18, 'add_eventhappened');
INSERT INTO auth_permission VALUES (53, 'Can change event happened', 18, 'change_eventhappened');
INSERT INTO auth_permission VALUES (54, 'Can delete event happened', 18, 'delete_eventhappened');
INSERT INTO auth_permission VALUES (55, 'Can add visit', 19, 'add_visit');
INSERT INTO auth_permission VALUES (56, 'Can change visit', 19, 'change_visit');
INSERT INTO auth_permission VALUES (57, 'Can delete visit', 19, 'delete_visit');
INSERT INTO auth_permission VALUES (58, 'Can add visit', 20, 'add_visit');
INSERT INTO auth_permission VALUES (59, 'Can change visit', 20, 'change_visit');
INSERT INTO auth_permission VALUES (60, 'Can delete visit', 20, 'delete_visit');
INSERT INTO auth_permission VALUES (61, 'Can add role', 21, 'add_role');
INSERT INTO auth_permission VALUES (62, 'Can change role', 21, 'change_role');
INSERT INTO auth_permission VALUES (63, 'Can delete role', 21, 'delete_role');
INSERT INTO auth_permission VALUES (64, 'Can add event', 22, 'add_event');
INSERT INTO auth_permission VALUES (65, 'Can change event', 22, 'change_event');
INSERT INTO auth_permission VALUES (66, 'Can delete event', 22, 'delete_event');
INSERT INTO auth_permission VALUES (67, 'Can add event happened', 23, 'add_eventhappened');
INSERT INTO auth_permission VALUES (68, 'Can change event happened', 23, 'change_eventhappened');
INSERT INTO auth_permission VALUES (69, 'Can delete event happened', 23, 'delete_eventhappened');
INSERT INTO auth_permission VALUES (70, 'Can add client', 24, 'add_client');
INSERT INTO auth_permission VALUES (71, 'Can change client', 24, 'change_client');
INSERT INTO auth_permission VALUES (72, 'Can delete client', 24, 'delete_client');


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: crmadmin
--

SELECT pg_catalog.setval('auth_permission_id_seq', 72, true);


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: crmadmin
--

INSERT INTO auth_user VALUES (2, 'pbkdf2_sha256$36000$VJDPHAbCFTcQ$Hc6GhXiE1sWPbR6MPvB5jE7Oth0iStxf2ilnQBaAZNE=', NULL, false, 'testManager', '', '', '', false, true, '2017-05-02 21:13:09.675859+09');
INSERT INTO auth_user VALUES (3, 'pbkdf2_sha256$36000$jHB13l2w7WYq$+vL46z9ImP+xLMhJbKJrZWgCE+swl9NHPNB2cxqGETs=', NULL, false, 'testDirector', '', '', '', false, true, '2017-05-02 21:14:18.576144+09');
INSERT INTO auth_user VALUES (1, 'pbkdf2_sha256$36000$ss2QF51v2pmT$qemw4iwUK9yhJSnYZ1dg+v93DoszEf802pmDqFCzdW8=', '2017-05-09 20:55:37.904847+09', true, 'leon', '', '', '', true, true, '2017-04-24 22:23:00.343551+09');


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: crmadmin
--



--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: crmadmin
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: crmadmin
--

SELECT pg_catalog.setval('auth_user_id_seq', 3, true);


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: crmadmin
--



--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: crmadmin
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 1, false);


--
-- Name: clManager_role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: crmadmin
--

SELECT pg_catalog.setval('"clManager_role_id_seq"', 9, true);


--
-- Data for Name: client; Type: TABLE DATA; Schema: public; Owner: crmadmin
--

INSERT INTO client VALUES (1, 1, 'dev', 'Егоров Антон', 'supervisor', '2017-04-24', '1994-09-03', NULL, NULL);
INSERT INTO client VALUES (2, 2, 'owner', 'Иванов Сергей Игоревич', 'owner', '2017-04-24', '1969-12-08', NULL, NULL);
INSERT INTO client VALUES (3, 3, 'oleg', 'Олег', 'oleg', '2017-04-24', '1970-09-21', NULL, NULL);
INSERT INTO client VALUES (4, 4, 'ivan', 'Иван', 'ivan', '2017-04-24', '1970-03-15', NULL, NULL);
INSERT INTO client VALUES (5, 3, 'masha', 'Мария', 'masha', '2017-04-24', '1974-03-22', NULL, NULL);


--
-- Data for Name: crm_client; Type: TABLE DATA; Schema: public; Owner: crmadmin
--

INSERT INTO crm_client VALUES (2, 'аноним', 'unknown', NULL, NULL, NULL, NULL, 'unknown@mail.com', NULL, 5);
INSERT INTO crm_client VALUES (4, 'Тимур', NULL, NULL, '2017-05-10', NULL, NULL, NULL, NULL, 5);
INSERT INTO crm_client VALUES (5, 'Юрий', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5);
INSERT INTO crm_client VALUES (1, 'Антон_2', NULL, NULL, '2017-05-17', '1994-05-09', NULL, '47664766@mail.ru', NULL, 5);


--
-- Name: crm_client_id_seq; Type: SEQUENCE SET; Schema: public; Owner: crmadmin
--

SELECT pg_catalog.setval('crm_client_id_seq', 5, true);


--
-- Data for Name: crm_event; Type: TABLE DATA; Schema: public; Owner: crmadmin
--

INSERT INTO crm_event VALUES (1, 'Обычное посещение', '50р. вход', NULL, NULL);
INSERT INTO crm_event VALUES (2, 'Мафия', '50р.+50р.', NULL, NULL);
INSERT INTO crm_event VALUES (4, 'Секретный гитлер', 'Мафия для политики', NULL, '');
INSERT INTO crm_event VALUES (5, 'MTG - Standard Showdown', 'Нужно описание', NULL, '');
INSERT INTO crm_event VALUES (3, 'MTG - Стандарт', 'Карточная коллекционная игра', NULL, '');


--
-- Name: crm_event_id_seq; Type: SEQUENCE SET; Schema: public; Owner: crmadmin
--

SELECT pg_catalog.setval('crm_event_id_seq', 5, true);


--
-- Data for Name: crm_role; Type: TABLE DATA; Schema: public; Owner: crmadmin
--

INSERT INTO crm_role VALUES (5, 'common_visitor', 1);
INSERT INTO crm_role VALUES (8, 'Завсегдатай', 2);
INSERT INTO crm_role VALUES (9, 'anonim', 0);


--
-- Data for Name: crm_visit; Type: TABLE DATA; Schema: public; Owner: crmadmin
--

INSERT INTO crm_visit VALUES (3, '2017-05-11', '', 2, 1);
INSERT INTO crm_visit VALUES (4, '2017-05-11', '', 1, 1);
INSERT INTO crm_visit VALUES (5, '2017-05-11', '', 1, 1);
INSERT INTO crm_visit VALUES (7, '2017-05-11', NULL, 4, 2);
INSERT INTO crm_visit VALUES (9, '2017-05-11', 'ничего не делал', 1, 1);
INSERT INTO crm_visit VALUES (10, '2017-05-11', NULL, 2, 2);
INSERT INTO crm_visit VALUES (11, '2017-05-01', NULL, 1, 1);
INSERT INTO crm_visit VALUES (12, '2017-05-14', NULL, 2, 1);
INSERT INTO crm_visit VALUES (13, '2017-05-14', NULL, 4, 2);
INSERT INTO crm_visit VALUES (14, '2017-05-14', NULL, 2, 4);
INSERT INTO crm_visit VALUES (15, '2017-05-14', NULL, 2, 1);
INSERT INTO crm_visit VALUES (16, '2017-05-14', NULL, 4, 1);
INSERT INTO crm_visit VALUES (17, '2017-05-14', NULL, 4, 1);
INSERT INTO crm_visit VALUES (18, '2017-05-14', NULL, 4, 2);
INSERT INTO crm_visit VALUES (19, '2017-05-14', NULL, 4, 2);
INSERT INTO crm_visit VALUES (20, '2017-05-14', NULL, 5, 2);
INSERT INTO crm_visit VALUES (21, '2017-05-14', NULL, 5, 2);
INSERT INTO crm_visit VALUES (22, '2017-05-14', NULL, 5, 2);
INSERT INTO crm_visit VALUES (23, '2017-05-14', NULL, 5, 2);
INSERT INTO crm_visit VALUES (24, '2017-05-14', NULL, 5, 2);
INSERT INTO crm_visit VALUES (25, '2017-05-15', NULL, 2, 2);
INSERT INTO crm_visit VALUES (26, '2017-05-15', NULL, 4, 2);
INSERT INTO crm_visit VALUES (27, '2017-05-15', NULL, 1, 2);
INSERT INTO crm_visit VALUES (28, '2017-05-15', NULL, 5, 2);
INSERT INTO crm_visit VALUES (29, '2017-05-16', NULL, 2, 2);
INSERT INTO crm_visit VALUES (30, '2017-05-16', NULL, 2, 2);
INSERT INTO crm_visit VALUES (31, '2017-05-16', NULL, 2, 2);
INSERT INTO crm_visit VALUES (32, '2017-05-16', NULL, 4, 1);
INSERT INTO crm_visit VALUES (33, '2017-05-16', NULL, 1, 1);
INSERT INTO crm_visit VALUES (34, '2017-05-16', NULL, 1, 2);
INSERT INTO crm_visit VALUES (35, '2017-05-16', NULL, 4, 5);
INSERT INTO crm_visit VALUES (36, '2017-05-16', NULL, 4, 3);
INSERT INTO crm_visit VALUES (37, '2017-05-16', NULL, 2, 3);
INSERT INTO crm_visit VALUES (38, '2017-05-21', NULL, 4, 1);
INSERT INTO crm_visit VALUES (39, '2017-05-21', NULL, 4, 4);
INSERT INTO crm_visit VALUES (40, '2017-05-21', NULL, 4, 3);


--
-- Name: crm_visit_id_seq; Type: SEQUENCE SET; Schema: public; Owner: crmadmin
--

SELECT pg_catalog.setval('crm_visit_id_seq', 40, true);


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: crmadmin
--

INSERT INTO django_admin_log VALUES (1, '2017-04-25 14:23:21.866821+09', '1', 'what''s up?', 2, '[]', 8, 1);
INSERT INTO django_admin_log VALUES (2, '2017-04-25 15:17:39.371053+09', '2', 'what colors do you prefer', 1, '[{"added": {}}]', 8, 1);
INSERT INTO django_admin_log VALUES (3, '2017-04-25 15:17:42.003687+09', '2', 'what colors do you prefer', 2, '[]', 8, 1);
INSERT INTO django_admin_log VALUES (4, '2017-04-25 15:17:50.752229+09', '2', 'what colors do you prefer?', 2, '[{"changed": {"fields": ["question_text"]}}]', 8, 1);
INSERT INTO django_admin_log VALUES (5, '2017-04-25 22:04:15.41603+09', '1', 'test1', 1, '[{"added": {}}]', 9, 1);
INSERT INTO django_admin_log VALUES (6, '2017-04-25 22:04:28.190173+09', '2', 'test2', 1, '[{"added": {}}]', 9, 1);
INSERT INTO django_admin_log VALUES (7, '2017-04-25 22:04:44.877335+09', '3', 'test3', 1, '[{"added": {}}]', 9, 1);
INSERT INTO django_admin_log VALUES (8, '2017-04-25 22:04:59.326138+09', '4', 'test4', 1, '[{"added": {}}]', 9, 1);
INSERT INTO django_admin_log VALUES (9, '2017-05-02 21:13:09.72323+09', '2', 'testManager', 1, '[{"added": {}}]', 4, 1);
INSERT INTO django_admin_log VALUES (10, '2017-05-02 21:14:18.606953+09', '3', 'testDirector', 1, '[{"added": {}}]', 4, 1);
INSERT INTO django_admin_log VALUES (11, '2017-05-09 00:01:32.460548+09', '5', 'common_visitor', 1, '[{"added": {}}]', 21, 1);
INSERT INTO django_admin_log VALUES (12, '2017-05-09 00:26:25.244228+09', '7', 'певец', 1, '[{"added": {}}]', 21, 1);
INSERT INTO django_admin_log VALUES (13, '2017-05-10 23:29:59.76968+09', '1', 'anton_strange', 1, '[{"added": {}}]', 24, 1);
INSERT INTO django_admin_log VALUES (14, '2017-05-10 23:31:59.827662+09', '2', 'аноним', 1, '[{"added": {}}]', 24, 1);
INSERT INTO django_admin_log VALUES (15, '2017-05-10 23:33:52.868535+09', '3', 'Олег_помошник', 1, '[{"added": {}}]', 24, 1);
INSERT INTO django_admin_log VALUES (16, '2017-05-10 23:34:10.660296+09', '3', 'Олег_помошник', 3, '', 24, 1);
INSERT INTO django_admin_log VALUES (17, '2017-05-10 23:34:44.202683+09', '4', 'Тимур', 1, '[{"added": {}}]', 24, 1);
INSERT INTO django_admin_log VALUES (18, '2017-05-10 23:43:04.83023+09', '5', 'Юрий_мотыга', 1, '[{"added": {}}]', 24, 1);
INSERT INTO django_admin_log VALUES (19, '2017-05-10 23:43:37.598118+09', '1', 'Обычное посещение', 1, '[{"added": {}}]', 22, 1);
INSERT INTO django_admin_log VALUES (20, '2017-05-10 23:44:13.50929+09', '2', 'Мафия', 1, '[{"added": {}}]', 22, 1);
INSERT INTO django_admin_log VALUES (21, '2017-05-11 09:50:58.300761+09', '5', 'EventHappened object', 1, '[{"added": {}}]', 23, 1);
INSERT INTO django_admin_log VALUES (22, '2017-05-11 09:53:37.195938+09', '7', 'EventHappened object', 1, '[{"added": {}}]', 23, 1);
INSERT INTO django_admin_log VALUES (23, '2017-05-11 09:59:50.830016+09', '3', '', 1, '[{"added": {}}]', 20, 1);
INSERT INTO django_admin_log VALUES (24, '2017-05-11 10:33:31.10799+09', '6', '', 3, '', 20, 1);
INSERT INTO django_admin_log VALUES (25, '2017-05-11 10:47:25.271542+09', '7', 'певец', 3, '', 21, 1);
INSERT INTO django_admin_log VALUES (26, '2017-05-11 10:47:49.745064+09', '8', 'Завсегдатай', 1, '[{"added": {}}]', 21, 1);
INSERT INTO django_admin_log VALUES (27, '2017-05-11 11:52:53.10186+09', '3', 'Magic the Gathering', 1, '[{"added": {}}]', 22, 1);
INSERT INTO django_admin_log VALUES (28, '2017-05-11 23:16:20.889873+09', '3', 'Magic the Gathering - Стандарт', 2, '[{"changed": {"fields": ["name"]}}]', 22, 1);
INSERT INTO django_admin_log VALUES (29, '2017-05-11 23:17:30.835194+09', '5', 'Юрий', 2, '[{"changed": {"fields": ["login"]}}]', 24, 1);
INSERT INTO django_admin_log VALUES (30, '2017-05-11 23:18:54.385041+09', '4', 'Секретный гитлер', 1, '[{"added": {}}]', 22, 1);
INSERT INTO django_admin_log VALUES (31, '2017-05-11 23:19:42.203587+09', '1', 'Антон_2', 2, '[{"changed": {"fields": ["login"]}}]', 24, 1);
INSERT INTO django_admin_log VALUES (32, '2017-05-11 23:23:19.764048+09', '5', 'MTG - Standard Showdown', 1, '[{"added": {}}]', 22, 1);
INSERT INTO django_admin_log VALUES (33, '2017-05-11 23:23:30.409827+09', '3', 'MTG - Стандарт', 2, '[{"changed": {"fields": ["name"]}}]', 22, 1);
INSERT INTO django_admin_log VALUES (34, '2017-05-11 23:25:02.793761+09', '11', '11', 1, '[{"added": {}}]', 20, 1);
INSERT INTO django_admin_log VALUES (35, '2017-05-16 13:24:44.084343+09', '9', 'anonim', 1, '[{"added": {}}]', 21, 1);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: crmadmin
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 35, true);


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: crmadmin
--

INSERT INTO django_content_type VALUES (1, 'admin', 'logentry');
INSERT INTO django_content_type VALUES (2, 'auth', 'group');
INSERT INTO django_content_type VALUES (3, 'auth', 'permission');
INSERT INTO django_content_type VALUES (4, 'auth', 'user');
INSERT INTO django_content_type VALUES (5, 'contenttypes', 'contenttype');
INSERT INTO django_content_type VALUES (6, 'sessions', 'session');
INSERT INTO django_content_type VALUES (7, 'polls', 'choice');
INSERT INTO django_content_type VALUES (8, 'polls', 'question');
INSERT INTO django_content_type VALUES (9, 'welcome', 'post');
INSERT INTO django_content_type VALUES (10, 'clManager', 'clients');
INSERT INTO django_content_type VALUES (11, 'clManager', 'roles');
INSERT INTO django_content_type VALUES (12, 'clManager', 'visits');
INSERT INTO django_content_type VALUES (13, 'clManager', 'eventshappened');
INSERT INTO django_content_type VALUES (14, 'clManager', 'events');
INSERT INTO django_content_type VALUES (15, 'clManager', 'role');
INSERT INTO django_content_type VALUES (16, 'clManager', 'client');
INSERT INTO django_content_type VALUES (17, 'clManager', 'event');
INSERT INTO django_content_type VALUES (18, 'clManager', 'eventhappened');
INSERT INTO django_content_type VALUES (19, 'clManager', 'visit');
INSERT INTO django_content_type VALUES (20, 'crm', 'visit');
INSERT INTO django_content_type VALUES (21, 'crm', 'role');
INSERT INTO django_content_type VALUES (22, 'crm', 'event');
INSERT INTO django_content_type VALUES (23, 'crm', 'eventhappened');
INSERT INTO django_content_type VALUES (24, 'crm', 'client');


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: crmadmin
--

SELECT pg_catalog.setval('django_content_type_id_seq', 24, true);


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: crmadmin
--

INSERT INTO django_migrations VALUES (1, 'contenttypes', '0001_initial', '2017-04-24 22:22:16.189145+09');
INSERT INTO django_migrations VALUES (2, 'auth', '0001_initial', '2017-04-24 22:22:17.132176+09');
INSERT INTO django_migrations VALUES (3, 'admin', '0001_initial', '2017-04-24 22:22:17.342655+09');
INSERT INTO django_migrations VALUES (4, 'admin', '0002_logentry_remove_auto_add', '2017-04-24 22:22:17.375774+09');
INSERT INTO django_migrations VALUES (5, 'contenttypes', '0002_remove_content_type_name', '2017-04-24 22:22:17.4088+09');
INSERT INTO django_migrations VALUES (6, 'auth', '0002_alter_permission_name_max_length', '2017-04-24 22:22:17.431212+09');
INSERT INTO django_migrations VALUES (7, 'auth', '0003_alter_user_email_max_length', '2017-04-24 22:22:17.453355+09');
INSERT INTO django_migrations VALUES (8, 'auth', '0004_alter_user_username_opts', '2017-04-24 22:22:17.471419+09');
INSERT INTO django_migrations VALUES (9, 'auth', '0005_alter_user_last_login_null', '2017-04-24 22:22:17.545302+09');
INSERT INTO django_migrations VALUES (10, 'auth', '0006_require_contenttypes_0002', '2017-04-24 22:22:17.563721+09');
INSERT INTO django_migrations VALUES (11, 'auth', '0007_alter_validators_add_error_messages', '2017-04-24 22:22:17.583314+09');
INSERT INTO django_migrations VALUES (12, 'auth', '0008_alter_user_username_max_length', '2017-04-24 22:22:17.663598+09');
INSERT INTO django_migrations VALUES (13, 'sessions', '0001_initial', '2017-04-24 22:22:17.896446+09');
INSERT INTO django_migrations VALUES (14, 'polls', '0001_initial', '2017-04-25 12:02:44.396056+09');
INSERT INTO django_migrations VALUES (15, 'welcome', '0001_initial', '2017-04-25 22:01:13.226605+09');
INSERT INTO django_migrations VALUES (16, 'clManager', '0001_initial', '2017-05-02 22:26:56.584528+09');
INSERT INTO django_migrations VALUES (17, 'clManager', '0002_roles', '2017-05-02 23:04:12.632509+09');
INSERT INTO django_migrations VALUES (18, 'clManager', '0003_auto_20170505_1427', '2017-05-05 23:27:03.749334+09');
INSERT INTO django_migrations VALUES (19, 'clManager', '0002_auto_20170508_1449', '2017-05-08 23:49:46.093551+09');
INSERT INTO django_migrations VALUES (20, 'crm', '0001_initial', '2017-05-08 23:54:36.324212+09');
INSERT INTO django_migrations VALUES (21, 'crm', '0002_auto_20170508_1459', '2017-05-08 23:59:58.240841+09');
INSERT INTO django_migrations VALUES (22, 'crm', '0003_auto_20170510_1432', '2017-05-10 23:32:39.474654+09');
INSERT INTO django_migrations VALUES (23, 'crm', '0004_auto_20170511_0048', '2017-05-11 09:48:53.115934+09');
INSERT INTO django_migrations VALUES (24, 'crm', '0005_auto_20170511_0057', '2017-05-11 09:57:44.166915+09');
INSERT INTO django_migrations VALUES (25, 'crm', '0006_auto_20170511_0058', '2017-05-11 09:58:55.402412+09');
INSERT INTO django_migrations VALUES (26, 'crm', '0007_auto_20170511_1421', '2017-05-11 23:21:58.263449+09');


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: crmadmin
--

SELECT pg_catalog.setval('django_migrations_id_seq', 26, true);


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: crmadmin
--

INSERT INTO django_session VALUES ('l4a2fxtla9f1u1p4dycyezb1tdkm4n1f', 'YjNlOTgwODNiMjcxODg2ZGM3OWY5ZWM3MTYyMWE1ZjdhYTk2MTJiZTp7Il9hdXRoX3VzZXJfaGFzaCI6IjZmMzA0M2RkMzU1YzYwZmY1OWZlYjM5MDhlNzFhMTMwNWE0ZDYwMzQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2017-05-09 12:59:11.137039+09');
INSERT INTO django_session VALUES ('h17cdykhi53ry3vopshdxjurlgmzjqoz', 'YjNlOTgwODNiMjcxODg2ZGM3OWY5ZWM3MTYyMWE1ZjdhYTk2MTJiZTp7Il9hdXRoX3VzZXJfaGFzaCI6IjZmMzA0M2RkMzU1YzYwZmY1OWZlYjM5MDhlNzFhMTMwNWE0ZDYwMzQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2017-05-23 20:55:37.943502+09');


--
-- Data for Name: polls_choice; Type: TABLE DATA; Schema: public; Owner: crmadmin
--

INSERT INTO polls_choice VALUES (1, 'Not much', 7, 1);
INSERT INTO polls_choice VALUES (2, 'The sky', 5, 1);
INSERT INTO polls_choice VALUES (3, 'Just hacking again', 10, 1);


--
-- Name: polls_choice_id_seq; Type: SEQUENCE SET; Schema: public; Owner: crmadmin
--

SELECT pg_catalog.setval('polls_choice_id_seq', 3, true);


--
-- Data for Name: polls_question; Type: TABLE DATA; Schema: public; Owner: crmadmin
--

INSERT INTO polls_question VALUES (1, 'what''s up?', '2017-04-25 12:09:57+09');
INSERT INTO polls_question VALUES (2, 'what colors do you prefer?', '2017-04-25 15:17:23+09');


--
-- Name: polls_question_id_seq; Type: SEQUENCE SET; Schema: public; Owner: crmadmin
--

SELECT pg_catalog.setval('polls_question_id_seq', 2, true);


--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: crmadmin
--

INSERT INTO roles VALUES (1, 'dev', 1);
INSERT INTO roles VALUES (2, 'owner', 2);
INSERT INTO roles VALUES (3, 'manager', 3);
INSERT INTO roles VALUES (4, 'emploee', 4);


--
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: crmadmin
--

SELECT pg_catalog.setval('roles_id_seq', 1, false);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: crmadmin
--

SELECT pg_catalog.setval('users_id_seq', 1, false);


--
-- Data for Name: welcome_post; Type: TABLE DATA; Schema: public; Owner: crmadmin
--

INSERT INTO welcome_post VALUES (1, 'test1', 'text1', '2017-04-25 22:03:36+09', '2017-04-25 22:04:10+09', 1);
INSERT INTO welcome_post VALUES (2, 'test2', 'text2', '2017-04-25 22:04:15+09', '2017-04-25 22:04:27+09', 1);
INSERT INTO welcome_post VALUES (3, 'test3', 'text3', '2017-04-25 22:04:28+09', NULL, 1);
INSERT INTO welcome_post VALUES (4, 'test4', 'text4', '2017-04-25 22:04:44+09', NULL, 1);


--
-- Name: welcome_post_id_seq; Type: SEQUENCE SET; Schema: public; Owner: crmadmin
--

SELECT pg_catalog.setval('welcome_post_id_seq', 4, true);


--
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: crmadmin; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: crmadmin; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: crmadmin; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: crmadmin; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: crmadmin; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: crmadmin; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: crmadmin; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: crmadmin; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: crmadmin; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: crmadmin; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: crmadmin; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: crmadmin; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: clManager_role_pkey; Type: CONSTRAINT; Schema: public; Owner: crmadmin; Tablespace: 
--

ALTER TABLE ONLY crm_role
    ADD CONSTRAINT "clManager_role_pkey" PRIMARY KEY (id);


--
-- Name: crm_client_pkey; Type: CONSTRAINT; Schema: public; Owner: crmadmin; Tablespace: 
--

ALTER TABLE ONLY crm_client
    ADD CONSTRAINT crm_client_pkey PRIMARY KEY (id);


--
-- Name: crm_event_pkey; Type: CONSTRAINT; Schema: public; Owner: crmadmin; Tablespace: 
--

ALTER TABLE ONLY crm_event
    ADD CONSTRAINT crm_event_pkey PRIMARY KEY (id);


--
-- Name: crm_visit_pkey; Type: CONSTRAINT; Schema: public; Owner: crmadmin; Tablespace: 
--

ALTER TABLE ONLY crm_visit
    ADD CONSTRAINT crm_visit_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: crmadmin; Tablespace: 
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: crmadmin; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: crmadmin; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: crmadmin; Tablespace: 
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: crmadmin; Tablespace: 
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: polls_choice_pkey; Type: CONSTRAINT; Schema: public; Owner: crmadmin; Tablespace: 
--

ALTER TABLE ONLY polls_choice
    ADD CONSTRAINT polls_choice_pkey PRIMARY KEY (id);


--
-- Name: polls_question_pkey; Type: CONSTRAINT; Schema: public; Owner: crmadmin; Tablespace: 
--

ALTER TABLE ONLY polls_question
    ADD CONSTRAINT polls_question_pkey PRIMARY KEY (id);


--
-- Name: roles_pkey; Type: CONSTRAINT; Schema: public; Owner: crmadmin; Tablespace: 
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: crmadmin; Tablespace: 
--

ALTER TABLE ONLY client
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: welcome_post_pkey; Type: CONSTRAINT; Schema: public; Owner: crmadmin; Tablespace: 
--

ALTER TABLE ONLY welcome_post
    ADD CONSTRAINT welcome_post_pkey PRIMARY KEY (id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE INDEX auth_group_name_a6ea08ec_like ON auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE INDEX auth_user_groups_group_id_97559544 ON auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE INDEX auth_user_username_6821ab7c_like ON auth_user USING btree (username varchar_pattern_ops);


--
-- Name: crm_client_id_role_id_4e3999ca; Type: INDEX; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE INDEX crm_client_id_role_id_4e3999ca ON crm_client USING btree (id_role_id);


--
-- Name: crm_visit_id_client_id_2056817e; Type: INDEX; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE INDEX crm_visit_id_client_id_2056817e ON crm_visit USING btree (id_client_id);


--
-- Name: crm_visit_id_event_id_8db3837f; Type: INDEX; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE INDEX crm_visit_id_event_id_8db3837f ON crm_visit USING btree (id_event_id);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE INDEX django_session_expire_date_a5c62663 ON django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE INDEX django_session_session_key_c0390e0f_like ON django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: polls_choice_question_id_c5b4b260; Type: INDEX; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE INDEX polls_choice_question_id_c5b4b260 ON polls_choice USING btree (question_id);


--
-- Name: welcome_post_author_id_3a3f7e4b; Type: INDEX; Schema: public; Owner: crmadmin; Tablespace: 
--

CREATE INDEX welcome_post_author_id_3a3f7e4b ON welcome_post USING btree (author_id);


--
-- Name: auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: crmadmin
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: crmadmin
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: crmadmin
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: crmadmin
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: crmadmin
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: crmadmin
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: crmadmin
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: crm_client_id_role_id_4e3999ca_fk_clManager_role_id; Type: FK CONSTRAINT; Schema: public; Owner: crmadmin
--

ALTER TABLE ONLY crm_client
    ADD CONSTRAINT "crm_client_id_role_id_4e3999ca_fk_clManager_role_id" FOREIGN KEY (id_role_id) REFERENCES crm_role(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: crm_visit_id_client_id_2056817e_fk_crm_client_id; Type: FK CONSTRAINT; Schema: public; Owner: crmadmin
--

ALTER TABLE ONLY crm_visit
    ADD CONSTRAINT crm_visit_id_client_id_2056817e_fk_crm_client_id FOREIGN KEY (id_client_id) REFERENCES crm_client(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: crm_visit_id_event_id_8db3837f_fk_crm_event_id; Type: FK CONSTRAINT; Schema: public; Owner: crmadmin
--

ALTER TABLE ONLY crm_visit
    ADD CONSTRAINT crm_visit_id_event_id_8db3837f_fk_crm_event_id FOREIGN KEY (id_event_id) REFERENCES crm_event(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: crmadmin
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: crmadmin
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: polls_choice_question_id_c5b4b260_fk_polls_question_id; Type: FK CONSTRAINT; Schema: public; Owner: crmadmin
--

ALTER TABLE ONLY polls_choice
    ADD CONSTRAINT polls_choice_question_id_c5b4b260_fk_polls_question_id FOREIGN KEY (question_id) REFERENCES polls_question(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: welcome_post_author_id_3a3f7e4b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: crmadmin
--

ALTER TABLE ONLY welcome_post
    ADD CONSTRAINT welcome_post_author_id_3a3f7e4b_fk_auth_user_id FOREIGN KEY (author_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

