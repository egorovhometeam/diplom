\echo Next connection info string
\conninfo
--\echo Set client encoding to WIN1251:
--\encoding WIN1251

-- искать ОШИБКА
-- postgresql port 5432

-- Database: vstamp

DROP DATABASE IF EXISTS galaxy;

-- create user vstamp
drop user if exists vstamp;
create user vstamp with password 'vstamp';

CREATE DATABASE galaxy
  WITH OWNER = vstamp
       TEMPLATE = template0
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'ru_RU.utf8'
       LC_CTYPE = 'ru_RU.utf8'
       CONNECTION LIMIT = -1;

--CREATE DATABASE galaxy
--  WITH OWNER = vstamp
--       ENCODING = 'UTF8'
--       TABLESPACE = pg_default
--       LC_COLLATE = 'Russian, Russia'
--       LC_CTYPE = 'Russian, Russia'
--       CONNECTION LIMIT = -1;

-- Next command not needed because vstamp is own of database
--grant all privileges on database galaxy to vstamp;

\c galaxy

-- Table: companies. Список собственных дочерних компаний.
-- Не нужна таблица. Просто использовать таблицу contragents
-- только использовать отдельную группу (свои компании).

--DROP TABLE IF EXISTS companies;

--CREATE TABLE IF NOT EXISTS companies
--(
--  id serial NOT NULL,
--  idtenant integer,
--  name character varying(500),
--  shortname character varying(50),
--  comments character varying(500),
--  CONSTRAINT companies_pkey PRIMARY KEY (id)
--)
--WITH (
--  OIDS=FALSE
--);
--ALTER TABLE companies
--  OWNER TO vstamp;


-- Table: contacts

DROP TABLE IF EXISTS contacts;

CREATE TABLE IF NOT EXISTS contacts
(
  id serial NOT NULL,
  idtenant integer,
  idcontacttype integer NOT NULL,
  value character varying(100),
  comments character varying(500),
  CONSTRAINT contacts_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE contacts
  OWNER TO vstamp;

-- Table: contacttypes

DROP TABLE IF EXISTS contacttypes;

CREATE TABLE IF NOT EXISTS contacttypes
(
  id serial NOT NULL,
  idtenant integer,
  name character varying(100),
  CONSTRAINT contacttypes_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE contacttypes
  OWNER TO vstamp;

-- Table: contragents (пока очень базовая структура)
-- В этой таблице собираем всех внешних контрагентов (покупателей, поставщиков, партнеров и т.д.), 
-- а также все собственные дочерние независимые компании. Разделение определяется таблицей group2ourcompany.

DROP TABLE IF EXISTS contragents;

CREATE TABLE IF NOT EXISTS contragents
(
  id serial NOT NULL,
  idtenant integer,
  name character varying(500),
  shortname character varying(50),
  comments character varying(500),
  CONSTRAINT contragents_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE contragents
  OWNER TO vstamp;

-- Table: currencies

DROP TABLE IF EXISTS currencies;

CREATE TABLE IF NOT EXISTS currencies 
(
  id serial NOT NULL,
  idtenant integer,
  name character varying(50),
  shortname character varying(10),
  abbreviation character varying(10),
  CONSTRAINT currencies_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE currencies
  OWNER TO vstamp;

-- Table: departments

DROP TABLE IF EXISTS departments;

CREATE TABLE IF NOT EXISTS departments
(
  id serial NOT NULL,
  idtenant integer,
  idcontragent integer NOT NULL,
  idparent integer,
  idroot integer NOT NULL,
  name character varying(100),
  CONSTRAINT departments_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE departments
  OWNER TO vstamp;

-- Table: group2ourcompany

DROP TABLE IF EXISTS group2ourcompany;

CREATE TABLE IF NOT EXISTS group2ourcompany
(
  idgroup integer NOT NULL,
  idcontragent integer NOT NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE group2ourcompany
  OWNER TO vstamp;
CREATE INDEX group2ourcompany_group_idx ON group2ourcompany (idgroup);
CREATE INDEX group2ourcompany_contragent_idx ON group2ourcompany (idcontragent);

-- Table: group2contact

DROP TABLE IF EXISTS group2contact;

CREATE TABLE IF NOT EXISTS group2contact
(
  idgroup integer NOT NULL,
  idcontact integer NOT NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE group2contact
  OWNER TO vstamp;
CREATE INDEX group2contact_group_idx ON group2contact (idgroup);
CREATE INDEX group2contact_contact_idx ON group2contact (idcontact);

-- Table: group2group

DROP TABLE IF EXISTS group2group;

CREATE TABLE IF NOT EXISTS group2group
(
  idgroup integer NOT NULL,
  idgroupitem integer NOT NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE group2group
  OWNER TO vstamp;
CREATE INDEX group2group_group_idx ON group2group (idgroup);
CREATE INDEX group2group_groupitem_idx ON group2group (idgroupitem);

-- Table: group2material

DROP TABLE IF EXISTS group2material;

CREATE TABLE IF NOT EXISTS group2material
(
  idgroup integer NOT NULL,
  idmaterial integer NOT NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE group2material
  OWNER TO vstamp;
CREATE INDEX group2material_group_idx ON group2material (idgroup);
CREATE INDEX group2material_material_idx ON group2material (idmaterial);

-- Table: group2measuretype

DROP TABLE IF EXISTS group2measuretype;

CREATE TABLE IF NOT EXISTS group2measuretype
(
  idgroup integer NOT NULL,
  idmeasuretype integer NOT NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE group2measuretype
  OWNER TO vstamp;
CREATE INDEX group2measuretype_group_idx ON group2measuretype (idgroup);
CREATE INDEX group2measuretype_measuretype_idx ON group2measuretype (idmeasuretype);

-- Table: group2model

DROP TABLE IF EXISTS group2model;

CREATE TABLE IF NOT EXISTS group2model
(
  idgroup integer NOT NULL,
  idmodel integer NOT NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE group2model
  OWNER TO vstamp;
CREATE INDEX group2model_group_idx ON group2model (idgroup);
CREATE INDEX group2model_model_idx ON group2model (idmodel);

-- Table: group2modelparam

DROP TABLE IF EXISTS group2modelparam;

CREATE TABLE IF NOT EXISTS group2modelparam
(
  idgroup integer NOT NULL,
  idmodelparam integer NOT NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE group2modelparam
  OWNER TO vstamp;
CREATE INDEX group2modelparam_group_idx ON group2modelparam (idgroup);
CREATE INDEX group2modelparam_modelparam_idx ON group2modelparam (idmodelparam);

-- Table: group2namegenerator

DROP TABLE IF EXISTS group2namegenerator;

CREATE TABLE IF NOT EXISTS group2namegenerator
(
  idgroup integer NOT NULL,
  idnamegenerator integer NOT NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE group2namegenerator
  OWNER TO vstamp;
CREATE INDEX group2namegenerator_group_idx ON group2namegenerator (idgroup);
CREATE INDEX group2namegenerator_namegenerator_idx ON group2namegenerator (idnamegenerator);

-- Table: group2right

DROP TABLE IF EXISTS group2right;

CREATE TABLE IF NOT EXISTS group2right
(
  idgroup integer NOT NULL,
  idrightroot integer NOT NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE group2right
  OWNER TO vstamp;
CREATE INDEX group2right_group_idx ON group2right (idgroup);
CREATE INDEX group2right_rightroot_idx ON group2right (idrightroot);

-- Table: group2operation

DROP TABLE IF EXISTS group2operation;

CREATE TABLE IF NOT EXISTS group2operation
(
  idgroup integer NOT NULL,
  idoperation integer NOT NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE group2operation
  OWNER TO vstamp;
CREATE INDEX group2operation_group_idx ON group2operation (idgroup);
CREATE INDEX group2operation_operation_idx ON group2operation (idoperation);

-- Table: group2state

DROP TABLE IF EXISTS group2state;

--CREATE TABLE IF NOT EXISTS group2state
--(
--  idgroup integer NOT NULL,
--  idstate integer NOT NULL
--)
--WITH (
--  OIDS=FALSE
--);
--ALTER TABLE group2state
--  OWNER TO vstamp;
--CREATE INDEX group2state_group_idx ON group2state (idgroup);
--CREATE INDEX group2state_state_idx ON group2state (idstate);

-- Table: groups

DROP TABLE IF EXISTS groups;

CREATE TABLE IF NOT EXISTS groups
(
  id serial NOT NULL,
  idtenant integer,
  idparent integer,
  idroot integer NOT NULL,
  name character varying(100),
  comments character varying(500),
  CONSTRAINT groups_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE groups
  OWNER TO vstamp;

-- Table: group2serial

DROP TABLE IF EXISTS group2serial;

CREATE TABLE IF NOT EXISTS group2serial
(
  idgroup integer NOT NULL,
  idfirst integer DEFAULT 0,					-- первый определяющий идентификатор (например, контрагент)
  idsecond integer DEFAULT 0,					-- второй определяющий идентификатор
  newserial integer NOT NULL DEFAULT 1,
  CONSTRAINT group2serial_pkey PRIMARY KEY (idgroup, idfirst, idsecond)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE group2serial
  OWNER TO vstamp;

-- Table: inputtypes
-- define type of input editor: value, list of values, date

DROP TABLE IF EXISTS inputtypes;

CREATE TABLE IF NOT EXISTS inputtypes
(
  id serial NOT NULL,
  name character varying(100),
  CONSTRAINT inputtypes_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE inputtypes
  OWNER TO vstamp;

-- Table: materials

DROP TABLE IF EXISTS materials;

CREATE TABLE IF NOT EXISTS materials
(
  id serial NOT NULL,
  idorig integer NOT NULL DEFAULT -1,					--start id for record (preserve a foreign referencies and history)
  deleted timestamp with time zone DEFAULT NULL,			--time when row was deleted
  delwhoid integer DEFAULT NULL,					--id of entity which do deletion (idpeople)
  modified timestamp with time zone DEFAULT now(),			--time when row was modified
  modwhoid integer DEFAULT NULL,					--id of entity which do modification (idpeople)
  remoteip character varying(100),					--ip address of remote client machine
  remotehostname character varying(100),				--hostname for remote client machine
  idtenant integer,
  marking character varying(100),
  name character varying(100),
  description character varying(500),
  idvendor integer NOT NULL,
  idmeasuretype integer NOT NULL,
  comments character varying(500),
  CONSTRAINT materials_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE materials
  OWNER TO vstamp;
CREATE INDEX materials_orig_idx ON materials (idorig);

DROP TRIGGER IF EXISTS trigger_materials_insert ON materials;
CREATE OR REPLACE FUNCTION insert_new_material() RETURNS TRIGGER AS
$body$
DECLARE
BEGIN
	IF (TG_OP = 'INSERT') THEN
		UPDATE materials SET idorig = NEW.id WHERE id = NEW.id;
	END IF;
	RETURN NEW;
END;
$body$
LANGUAGE plpgsql;
CREATE TRIGGER trigger_materials_insert AFTER INSERT ON materials FOR EACH ROW EXECUTE PROCEDURE insert_new_material();

-- Table: measures

DROP TABLE IF EXISTS measures;

CREATE TABLE IF NOT EXISTS measures
(
  id serial NOT NULL,
  idtenant integer,
  idmeasuretype integer,
  idbase integer,					-- ссылка на базовую единицу измерения
  factor real,						-- количественный коэффициент от базовой единицы измерения
  name character varying(100),
  CONSTRAINT measures_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE measures
  OWNER TO vstamp;
CREATE INDEX measures_measuretype_idx ON measures (idmeasuretype);

-- Table: measuretypes

DROP TABLE IF EXISTS measuretypes;

CREATE TABLE IF NOT EXISTS measuretypes
(
  id serial NOT NULL,
  idtenant integer,
  name character varying(100),
  comments character varying(500),
  CONSTRAINT measuretypes_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE measuretypes
  OWNER TO vstamp;

-- Table: modelparamitems

DROP TABLE IF EXISTS modelparamitems;

CREATE TABLE IF NOT EXISTS modelparamitems
(
  id serial NOT NULL,
  idorig integer NOT NULL DEFAULT -1,					--start id for record (preserve a foreign referencies and history)
  deleted timestamp with time zone DEFAULT NULL,			--time when row was deleted
  delwhoid integer DEFAULT NULL,					--id of entity which do deletion (idpeople)
  modified timestamp with time zone DEFAULT now(),			--time when row was modified
  modwhoid integer DEFAULT NULL,					--id of entity which do modification (idpeople)
  remoteip character varying(100),					--ip address of remote client machine
  remotehostname character varying(100),				--hostname for remote client machine
  idmodelparam integer NOT NULL,					-- ссылка на параметр модели
  idmeasuretype integer,
  idmeasuredefault integer,
  idinputtype integer,							--а нужен ли этот параметр?
  idgroup integer,
  name character varying(100),
  CONSTRAINT modelparamitems_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE modelparamitems
  OWNER TO vstamp;
CREATE INDEX modelparamitems_modelparam_idx ON modelparamitems (idmodelparam);
CREATE INDEX modelparamitems_orig_idx ON modelparamitems (idorig);

DROP TRIGGER IF EXISTS trigger_modelparamitems_insert ON modelparamitems;
CREATE OR REPLACE FUNCTION insert_new_modelparamitem() RETURNS TRIGGER AS
$body$
DECLARE
BEGIN
	IF (TG_OP = 'INSERT') THEN
		UPDATE modelparamitems SET idorig = NEW.id WHERE id = NEW.id;
	END IF;
	RETURN NEW;
END;
$body$
LANGUAGE plpgsql;
CREATE TRIGGER trigger_modelparamitems_insert AFTER INSERT ON modelparamitems FOR EACH ROW EXECUTE PROCEDURE insert_new_modelparamitem();

-- Table: modelparams
-- Составные части модели. Здесь важно указать тип параметра (материал, операция или полуфабрикат),
-- и указать тип измерения этого параметра. Нам ведь важно заполнить количественные характеристики
-- этого параметра. Это и определяет тип измерений.
-- Кроме этого параметры модели имеют стоимостное измерение. Причем это гл. отличие от
-- составляющих этого параметра. Например: параметр модели Материал, составляющие: артикул, длина, ширина

-- idmodelref определяет возможную ссылку на полуфабрикат. В модели нет количественной информации, которая определяется
-- только расчетом. Например, если в данной модели используется 3-4 и более полуфабрикатов одной модели - в параметрах модели
-- следует указать только одну ссылку на данный полуфабрикат.

DROP TABLE IF EXISTS modelparams;

CREATE TABLE IF NOT EXISTS modelparams
(
  id serial NOT NULL,
  idorig integer NOT NULL DEFAULT -1,					--start id for record (preserve a foreign referencies and history)
  deleted timestamp with time zone DEFAULT NULL,			--time when row was deleted
  delwhoid integer DEFAULT NULL,					--id of entity which do deletion (idpeople)
  modified timestamp with time zone DEFAULT now(),			--time when row was modified
  modwhoid integer DEFAULT NULL,					--id of entity which do modification (idpeople)
  remoteip character varying(100),					--ip address of remote client machine
  remotehostname character varying(100),				--hostname for remote client machine
  idmodel integer NOT NULL,						-- ссылка на модель
  idmodelref integer,							-- возможная ссылка на другую модель (полуфабрикат)
  name character varying(100),						-- наименование параметра
  comments character varying(500),
  CONSTRAINT modelparams_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE modelparams
  OWNER TO vstamp;
CREATE INDEX modelparams_model_idx ON modelparams (idmodel);
CREATE INDEX modelparams_orig_idx ON modelparams (idorig);

DROP TRIGGER IF EXISTS trigger_modelparams_insert ON modelparams;
CREATE OR REPLACE FUNCTION insert_new_modelparam() RETURNS TRIGGER AS
$body$
DECLARE
BEGIN
	IF (TG_OP = 'INSERT') THEN
		UPDATE modelparams SET idorig = NEW.id WHERE id = NEW.id;
	END IF;
	RETURN NEW;
END;
$body$
LANGUAGE plpgsql;
CREATE TRIGGER trigger_modelparams_insert AFTER INSERT ON modelparams FOR EACH ROW EXECUTE PROCEDURE insert_new_modelparam();

--CREATE TABLE IF NOT EXISTS modelparams
--(
--  id serial NOT NULL,
--  idmodel integer NOT NULL,				-- ссылка на модель
--  idparent integer,
--  idroot integer NOT NULL,
--  idmeasuretype integer,
--  idmeasuredefault integer,
--  idinputtype integer,
--  name character varying(100),				-- наименование параметра
--  comments character varying(500),
--  CONSTRAINT modelparams_pkey PRIMARY KEY (id)
--)
--WITH (
--  OIDS=FALSE
--);
--ALTER TABLE modelparams
--  OWNER TO vstamp;
--CREATE INDEX modelparams_model_idx ON modelparams (idmodel);

-- Table: modelparamtypes

--DROP TABLE IF EXISTS modelparamtypes;

--CREATE TABLE IF NOT EXISTS modelparamtypes
--(
--  id serial NOT NULL,
--  idtenant integer,
--  name character varying(100),
--  CONSTRAINT modelparamtypes_pkey PRIMARY KEY (id)
--)
--WITH (
--  OIDS=FALSE
--);
--ALTER TABLE modelparamtypes
--  OWNER TO vstamp;

-- Table: models

DROP TABLE IF EXISTS models;

CREATE TABLE IF NOT EXISTS models
(
  id serial NOT NULL,
  idorig integer NOT NULL DEFAULT -1,					--start id for record (preserve a foreign referencies and history)
  deleted timestamp with time zone DEFAULT NULL,			--time when row was deleted
  delwhoid integer DEFAULT NULL,					--id of entity which do deletion (idpeople)
  modified timestamp with time zone DEFAULT now(),			--time when row was modified
  modwhoid integer DEFAULT NULL,					--id of entity which do modification (idpeople)
  remoteip character varying(100),					--ip address of remote client machine
  remotehostname character varying(100),				--hostname for remote client machine
  idtenant integer,
  idmodeltype integer NOT NULL,
  name character varying(100),
  comments character varying(500),
  CONSTRAINT models_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE models
  OWNER TO vstamp;
CREATE INDEX models_orig_idx ON models (idorig);

DROP TRIGGER IF EXISTS trigger_models_insert ON models;
CREATE OR REPLACE FUNCTION insert_new_model() RETURNS TRIGGER AS
$body$
DECLARE
BEGIN
	IF (TG_OP = 'INSERT') THEN
		UPDATE models SET idorig = NEW.id WHERE id = NEW.id;
	END IF;
	RETURN NEW;
END;
$body$
LANGUAGE plpgsql;
CREATE TRIGGER trigger_models_insert AFTER INSERT ON models FOR EACH ROW EXECUTE PROCEDURE insert_new_model();

-- Table: modeltypes

DROP TABLE IF EXISTS modeltypes;

CREATE TABLE IF NOT EXISTS modeltypes
(
  id serial NOT NULL,
  idtenant integer,
  name character varying(100),						-- тип позиции (продукция, материал, услуга)
  CONSTRAINT modeltypes_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE modeltypes
  OWNER TO vstamp;


-- Table: namegenerator
-- Для генерации названия документа с последовательной нумерацией из таблицы sernumbers
-- id имеет разное значение в зависимости от группы idgroup. Например, для группы
-- contragents_orders id это контрагент.
DROP TABLE IF EXISTS namegenerator;
CREATE TABLE IF NOT EXISTS namegenerator
(
  id serial NOT NULL,
  idtenant integer,
  idgroup integer NOT NULL,
  prefix character varying(10),
  middle character varying(20),
  suffix character varying(10),
  CONSTRAINT namegenerator_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE namegenerator
  OWNER TO vstamp;
CREATE INDEX namegenerator_idgroup_idx ON namegenerator (idgroup);

-- ОПЕРАЦИИ. Пока структура таблиц не проработана до конца (20120824.0904)

-- Table: operationitems

DROP TABLE IF EXISTS operationitems;

CREATE TABLE IF NOT EXISTS operationitems
(
  id serial NOT NULL,
  idoperation integer NOT NULL,
  idoperationitemtype integer NOT NULL,
  idofsome integer,
  idmeasuretype integer NOT NULL,
  CONSTRAINT operationitems_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE operationitems
  OWNER TO vstamp;

-- Table: operationitemtypes

DROP TABLE IF EXISTS operationitemtypes;

CREATE TABLE IF NOT EXISTS operationitemtypes
(
  id serial NOT NULL,
  idtenant integer,
  name character varying(100),
  CONSTRAINT operationitemtypes_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE operationitemtypes
  OWNER TO vstamp;

-- Table: operations

DROP TABLE IF EXISTS operations;

CREATE TABLE IF NOT EXISTS operations
(
  id serial NOT NULL,
  idorig integer NOT NULL DEFAULT -1,					--start id for record (preserve a foreign referencies and history)
  deleted timestamp with time zone DEFAULT NULL,			--time when row was deleted
  delwhoid integer DEFAULT NULL,					--id of entity which do deletion (idpeople)
  modified timestamp with time zone DEFAULT now(),			--time when row was modified
  modwhoid integer DEFAULT NULL,					--id of entity which do modification (idpeople)
  remoteip character varying(100),					--ip address of remote client machine
  remotehostname character varying(100),				--hostname for remote client machine
  idtenant integer,
  marking character varying(100),
  name character varying(100),
  description character varying(500),
  comments character varying(500),
  CONSTRAINT operations_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE operations
  OWNER TO vstamp;
CREATE INDEX operations_orig_idx ON operations (idorig);

DROP TRIGGER IF EXISTS trigger_operations_insert ON operations;
CREATE OR REPLACE FUNCTION insert_new_operation() RETURNS TRIGGER AS
$body$
DECLARE
BEGIN
	IF (TG_OP = 'INSERT') THEN
		UPDATE operations SET idorig = NEW.id WHERE id = NEW.id;
	END IF;
	RETURN NEW;
END;
$body$
LANGUAGE plpgsql;
CREATE TRIGGER trigger_operations_insert AFTER INSERT ON operations FOR EACH ROW EXECUTE PROCEDURE insert_new_operation();

-- Table: orderitemcalcs (данные расчета по заказу по каждому параметру модели - калькуляция)

DROP TABLE IF EXISTS orderitemcalcs;

CREATE TABLE IF NOT EXISTS orderitemcalcs
(
  id serial NOT NULL,
  idorderitem integer NOT NULL,				-- ссылка на позицию в заказе
  idmodelparam integer,					-- ссылка на конкретный параметр модели
  sum double precision NOT NULL,
  sumtax double precision NOT NULL,
 CONSTRAINT orderitemcalcs_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE orderitemcalcs
  OWNER TO vstamp;

-- Table: orderitemmodelvalues. Входные данные параметров модели для расчета. 

DROP TABLE IF EXISTS orderitemmodelvalues;

CREATE TABLE IF NOT EXISTS orderitemmodelvalues
(
  id serial NOT NULL,
  idorig integer NOT NULL DEFAULT -1,					--start id for record (preserve a foreign referencies and history)
  deleted timestamp with time zone DEFAULT NULL,			--time when row was deleted
  delwhoid integer DEFAULT NULL,					--id of entity which do deletion (idpeople)
  modified timestamp with time zone DEFAULT now(),			--time when row was modified
  modwhoid integer DEFAULT NULL,					--id of entity which do modification (idpeople)
  remoteip character varying(100),					--ip address of remote client machine
  remotehostname character varying(100),				--hostname for remote client machine
  idorderitem integer NOT NULL,						-- ссылка на позицию в заказе
  idmodelparamitem integer NOT NULL,					-- ссылка на конкретный атрибут параметра модели
  idmeasure integer NOT NULL,						-- ссылка на единицу измерения данного параметра
  value character varying(500) NOT NULL,
 CONSTRAINT orderitemmodelvalues_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE orderitemmodelvalues
  OWNER TO vstamp;
CREATE INDEX orderitemmodelvalues_orig_idx ON orderitemmodelvalues (idorig);

DROP TRIGGER IF EXISTS trigger_orderitemmodelvalues_insert ON orderitemmodelvalues;
CREATE OR REPLACE FUNCTION insert_new_orderitemmodelvalue() RETURNS TRIGGER AS
$body$
DECLARE
BEGIN
	IF (TG_OP = 'INSERT') THEN
		UPDATE orderitemmodelvalues SET idorig = NEW.id WHERE id = NEW.id;
	END IF;
	RETURN NEW;
END;
$body$
LANGUAGE plpgsql;
CREATE TRIGGER trigger_orderitemmodelvalues_insert AFTER INSERT ON orderitemmodelvalues FOR EACH ROW EXECUTE PROCEDURE insert_new_orderitemmodelvalue();

-- Table: orderitems

DROP TABLE IF EXISTS orderitems;

CREATE TABLE IF NOT EXISTS orderitems
(
  id serial NOT NULL,
  idorig integer NOT NULL DEFAULT -1,					--start id for record (preserve a foreign referencies and history)
  deleted timestamp with time zone DEFAULT NULL,			--time when row was deleted
  delwhoid integer DEFAULT NULL,					--id of entity which do deletion (idpeople)
  modified timestamp with time zone DEFAULT now(),			--time when row was modified
  modwhoid integer DEFAULT NULL,					--id of entity which do modification (idpeople)
  remoteip character varying(100),					--ip address of remote client machine
  remotehostname character varying(100),				--hostname for remote client machine
  idorder integer,
  numpos integer NOT NULL,						-- номер позиции в заказе
  idmodel integer NOT NULL,
  name character varying(100),						-- название модели
  amount double precision NOT NULL,							-- кол-во данной позиции в заказе
  price double precision NOT NULL,							-- стоимость единицы позиции
  pricetax double precision NOT NULL,						-- налог в стоимости единицы позиции
  sum double precision NOT NULL,
  sumtax double precision NOT NULL,
  comments character varying(500),
  CONSTRAINT orderitems_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE orderitems
  OWNER TO vstamp;
CREATE INDEX orderitems_orig_idx ON orderitems (idorig);

DROP TRIGGER IF EXISTS trigger_orderitem_insert ON orderitems;
DROP TRIGGER IF EXISTS trigger_orderitems_insert ON orderitems;
CREATE OR REPLACE FUNCTION insert_new_orderitem() RETURNS TRIGGER AS
$body$
DECLARE
BEGIN
	IF (TG_OP = 'INSERT') THEN
		UPDATE orderitems SET idorig = NEW.id WHERE id = NEW.id;
	END IF;
	RETURN NEW;
END;
$body$
LANGUAGE plpgsql;
CREATE TRIGGER trigger_orderitems_insert AFTER INSERT ON orderitems FOR EACH ROW EXECUTE PROCEDURE insert_new_orderitem();

-- Table: orderitemtypes

--DROP TABLE IF EXISTS orderitemtypes;

--CREATE TABLE IF NOT EXISTS orderitemtypes
--(
--  id serial NOT NULL,
--  idtenant integer,
--  name character varying(100),						-- тип позиции (продукция, материал, услуга)
--  CONSTRAINT orderitemtypes_pkey PRIMARY KEY (id)
--)
--WITH (
--  OIDS=FALSE
--);
--ALTER TABLE orderitemtypes
--  OWNER TO vstamp;

-- Table: orders

DROP TABLE IF EXISTS orders CASCADE;

CREATE TABLE IF NOT EXISTS orders
(
  id serial NOT NULL,
  idorig integer NOT NULL DEFAULT -1,					--start id for record (preserve a foreign referencies and history)
  deleted timestamp with time zone DEFAULT NULL,			--time when row was deleted
  delwhoid integer DEFAULT NULL,					--id of entity which do deletion (idpeople)
  modified timestamp with time zone DEFAULT now(),			--time when row was modified
  modwhoid integer DEFAULT NULL,					--id of entity which do modification (idpeople)
  remoteip character varying(100),					--ip address of remote client machine
  remotehostname character varying(100),				--hostname for remote client machine
  idtenant integer,
  idcontragent integer NOT NULL,
  idworker integer NOT NULL,
  iddepartment integer NOT NULL,					--id department where order created
  dtcre timestamp with time zone NOT NULL DEFAULT now(),
  name character varying(100),						-- порядковый номер заказа
  sum double precision,
  sumtax double precision,
  dtout timestamp with time zone,					-- необходимая дата изготовления
  discount double precision,
  idstate integer NOT NULL,
  comments character varying(500),
  CONSTRAINT orders_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE orders
  OWNER TO vstamp;
CREATE INDEX orders_orig_idx ON orders (idorig);

DROP TRIGGER IF EXISTS trigger_orders_insert ON orders;
CREATE OR REPLACE FUNCTION insert_new_order() RETURNS TRIGGER AS
$body$
DECLARE
BEGIN
	IF (TG_OP = 'INSERT') THEN
		UPDATE orders SET idorig = NEW.id WHERE id = NEW.id;
	END IF;
	RETURN NEW;
END;
$body$
LANGUAGE plpgsql;
CREATE TRIGGER trigger_orders_insert AFTER INSERT ON orders FOR EACH ROW EXECUTE PROCEDURE insert_new_order();

-- Table: people2contragent

DROP TABLE IF EXISTS people2contragent;

CREATE TABLE IF NOT EXISTS people2contragent
(
  idpeople integer NOT NULL,
  idcontragent integer NOT NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE people2contragent
  OWNER TO vstamp;
CREATE INDEX people2contragent_people_idx ON people2contragent (idpeople);
CREATE INDEX people2contragent_contragent_idx ON people2contragent (idcontragent);

-- Table: people2department

DROP TABLE IF EXISTS people2department;

CREATE TABLE IF NOT EXISTS people2department
(
  idpeople integer NOT NULL,
  iddepartment integer NOT NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE people2department
  OWNER TO vstamp;
CREATE INDEX people2department_people_idx ON people2department (idpeople);
CREATE INDEX people2department_department_idx ON people2department (iddepartment);

-- Table: people2right

DROP TABLE IF EXISTS people2right;

CREATE TABLE IF NOT EXISTS people2right
(
  idpeople integer NOT NULL,
  idright integer NOT NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE people2right
  OWNER TO vstamp;
CREATE INDEX people2right_people_idx ON people2right (idpeople);
CREATE INDEX people2right_right_idx ON people2right (idright);

-- Table: people2role

DROP TABLE IF EXISTS people2role;

CREATE TABLE IF NOT EXISTS people2role
(
  idpeople integer NOT NULL,
  idrole integer NOT NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE people2role
  OWNER TO vstamp;
CREATE INDEX people2role_people_idx ON people2role (idpeople);
CREATE INDEX people2role_role_idx ON people2role (idrole);

-- Table: peoples

DROP TABLE IF EXISTS peoples;

CREATE TABLE IF NOT EXISTS peoples
(
  id serial NOT NULL,
  idorig integer NOT NULL DEFAULT -1,					--start id for record (preserve a foreign referencies and history)
  deleted timestamp with time zone DEFAULT NULL,			--time when row was deleted
  delwhoid integer DEFAULT NULL,					--id of entity which do deletion (idpeople)
  modified timestamp with time zone DEFAULT now(),			--time when row was modified
  modwhoid integer DEFAULT NULL,					--id of entity which do modification (idpeople)
  remoteip character varying(100),					--ip address of remote client machine
  remotehostname character varying(100),				--hostname for remote client machine
  idtenant integer,
  login character varying(20),
  password character varying(20),
  firstname character varying(100),
  middlename character varying(100),
  lastname character varying(100),
  birthday date,
  dtcre timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
  idcreator integer NOT NULL,						-- idpeople менеджера компании, создавшего запись
  idstate int NOT NULL DEFAULT 1,
  comments character varying(500),
  CONSTRAINT peoples_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE peoples
  OWNER TO vstamp;
CREATE INDEX peoples_orig_idx ON peoples (idorig);

DROP TRIGGER IF EXISTS trigger_peoples_insert ON peoples;
CREATE OR REPLACE FUNCTION insert_new_people() RETURNS TRIGGER AS
$body$
DECLARE
BEGIN
	IF (TG_OP = 'INSERT') THEN
		UPDATE peoples SET idorig = NEW.id WHERE id = NEW.id;
	END IF;
	RETURN NEW;
END;
$body$
LANGUAGE plpgsql;
CREATE TRIGGER trigger_peoples_insert AFTER INSERT ON peoples FOR EACH ROW EXECUTE PROCEDURE insert_new_people();

-- Table: prepacks (полуфабрикаты)

DROP TABLE IF EXISTS prepacks;

CREATE TABLE IF NOT EXISTS prepacks
(
  id serial NOT NULL,
  idtenant integer,
  idmodel integer NOT NULL,
  CONSTRAINT prepacks_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE prepacks
  OWNER TO vstamp;
CREATE INDEX prepacks_model_idx ON prepacks (idmodel);

-- Table: pricesbuy (закупочные цены)

DROP TABLE IF EXISTS prices;
DROP TABLE IF EXISTS pricesbuy;

CREATE TABLE IF NOT EXISTS pricesbuy
(
  id serial NOT NULL,
  idtenant integer,
  idcontragent integer NOT NULL,			-- продавец
  idmaterial integer NOT NULL,
  idmeasure integer NOT NULL,
  idcurrency integer NOT NULL,
  price double precision,
  CONSTRAINT pricesbuy_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE pricesbuy
  OWNER TO vstamp;
CREATE INDEX pricesbuy_material_idx ON pricesbuy (idmaterial);

-- Table: rights

DROP TABLE IF EXISTS rights;

CREATE TABLE IF NOT EXISTS rights
(
  id serial NOT NULL,
  idtenant integer,
  idparent integer,
  idroot integer NOT NULL,
  name character varying(100),
  CONSTRAINT rights_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE rights
  OWNER TO vstamp;

-- Table: roles

DROP TABLE IF EXISTS roles;

CREATE TABLE IF NOT EXISTS roles
(
  id serial NOT NULL,
  idtenant integer,
  name character varying(100),
  CONSTRAINT roles_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE roles
  OWNER TO vstamp;

-- Table: staff
-- удаляем эту таблицу. Будем использовать общую: people2contragent

--DROP TABLE IF EXISTS staff;

--CREATE TABLE IF NOT EXISTS staff
--(
--  id serial NOT NULL,
--  idtenant integer,
--  idcontragent integer NOT NULL,
--  CONSTRAINT staff_pkey PRIMARY KEY (id)
--)
--WITH (
--  OIDS=FALSE
--);
--ALTER TABLE staff
--  OWNER TO vstamp;

-- Table: states

DROP TABLE IF EXISTS states;

CREATE TABLE IF NOT EXISTS states
(
  id serial NOT NULL,
  idtenant integer,
  idgroup integer NOT NULL,
  name character varying(100),
  CONSTRAINT states_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE states
  OWNER TO vstamp;

--##############################################

ALTER TABLE contacts ADD CONSTRAINT fk_contacts_contacttype FOREIGN KEY (idcontacttype) REFERENCES contacttypes;
ALTER TABLE departments ADD CONSTRAINT fk_departments_contragent FOREIGN KEY (idcontragent) REFERENCES contragents;
ALTER TABLE group2ourcompany ADD CONSTRAINT fk_group2ourcompany_group FOREIGN KEY (idgroup) REFERENCES groups;
ALTER TABLE group2ourcompany ADD CONSTRAINT fk_group2ourcompany_contragent FOREIGN KEY (idcontragent) REFERENCES contragents;
ALTER TABLE group2contact ADD CONSTRAINT fk_group2contact_group FOREIGN KEY (idgroup) REFERENCES groups;
ALTER TABLE group2contact ADD CONSTRAINT fk_group2contact_contact FOREIGN KEY (idcontact) REFERENCES contacts;
ALTER TABLE group2group ADD CONSTRAINT fk_group2group_group FOREIGN KEY (idgroup) REFERENCES groups;
ALTER TABLE group2group ADD CONSTRAINT fk_group2group_groupitem FOREIGN KEY (idgroupitem) REFERENCES groups;
ALTER TABLE group2material ADD CONSTRAINT fk_group2material_group FOREIGN KEY (idgroup) REFERENCES groups;
ALTER TABLE group2material ADD CONSTRAINT fk_group2material_material FOREIGN KEY (idmaterial) REFERENCES groups;
ALTER TABLE group2measuretype ADD CONSTRAINT fk_group2measuretype_group FOREIGN KEY (idgroup) REFERENCES groups;
ALTER TABLE group2measuretype ADD CONSTRAINT fk_group2measuretype_measuretype FOREIGN KEY (idmeasuretype) REFERENCES groups;
ALTER TABLE group2model ADD CONSTRAINT fk_group2model_group FOREIGN KEY (idgroup) REFERENCES groups;
ALTER TABLE group2model ADD CONSTRAINT fk_group2model_model FOREIGN KEY (idmodel) REFERENCES groups;
ALTER TABLE group2modelparam ADD CONSTRAINT fk_group2modelparam_group FOREIGN KEY (idgroup) REFERENCES groups;
ALTER TABLE group2modelparam ADD CONSTRAINT fk_group2modelparam_modelparam FOREIGN KEY (idmodelparam) REFERENCES groups;
ALTER TABLE group2namegenerator ADD CONSTRAINT fk_group2namegenerator_group FOREIGN KEY (idgroup) REFERENCES groups;
ALTER TABLE group2namegenerator ADD CONSTRAINT fk_group2namegenerator_namegenerator FOREIGN KEY (idnamegenerator) REFERENCES groups;
ALTER TABLE group2right ADD CONSTRAINT fk_group2right_group FOREIGN KEY (idgroup) REFERENCES groups;
ALTER TABLE group2right ADD CONSTRAINT fk_group2right_rightroot FOREIGN KEY (idrightroot) REFERENCES groups;
ALTER TABLE group2operation ADD CONSTRAINT fk_group2operation_group FOREIGN KEY (idgroup) REFERENCES groups;
ALTER TABLE group2operation ADD CONSTRAINT fk_group2operation_operation FOREIGN KEY (idoperation) REFERENCES groups;
ALTER TABLE group2serial ADD CONSTRAINT fk_group2serial_group FOREIGN KEY (idgroup) REFERENCES groups;
--ALTER TABLE group2state ADD CONSTRAINT fk_group2state_group FOREIGN KEY (idgroup) REFERENCES groups;
--ALTER TABLE group2state ADD CONSTRAINT fk_group2state_state FOREIGN KEY (idstate) REFERENCES states;

ALTER TABLE materials ADD CONSTRAINT fk_materials_vendor FOREIGN KEY (idvendor) REFERENCES contragents;
ALTER TABLE materials ADD CONSTRAINT fk_materials_measuretype FOREIGN KEY (idmeasuretype) REFERENCES measuretypes;
ALTER TABLE measures ADD CONSTRAINT fk_measures_measuretype FOREIGN KEY (idmeasuretype) REFERENCES measuretypes;
--ALTER TABLE models ADD CONSTRAINT fk_models_modelparam FOREIGN KEY (idmodelparam) REFERENCES modelparams;
ALTER TABLE models ADD CONSTRAINT fk_models_modeltype FOREIGN KEY (idmodeltype) REFERENCES modeltypes;
ALTER TABLE modelparams ADD CONSTRAINT fk_modelparams_model FOREIGN KEY (idmodel) REFERENCES models;
--ALTER TABLE modelparams ADD CONSTRAINT fk_modelparams_measuretype FOREIGN KEY (idmeasuretype) REFERENCES measuretypes;
--ALTER TABLE modelparams ADD CONSTRAINT fk_modelparams_inputtype FOREIGN KEY (idinputtype) REFERENCES inputtypes;
ALTER TABLE modelparamitems ADD CONSTRAINT fk_modelparamitems_modelparam FOREIGN KEY (idmodelparam) REFERENCES modelparams;
ALTER TABLE namegenerator ADD CONSTRAINT fk_namegenerator_group FOREIGN KEY (idgroup) REFERENCES groups;
ALTER TABLE operationitems ADD CONSTRAINT fk_operationitems_operation FOREIGN KEY (idoperation) REFERENCES operations;
ALTER TABLE operationitems ADD CONSTRAINT fk_operationitems_operationitemtype FOREIGN KEY (idoperationitemtype) REFERENCES operationitemtypes;
ALTER TABLE operationitems ADD CONSTRAINT fk_operationitems_measuretype FOREIGN KEY (idmeasuretype) REFERENCES measuretypes;

ALTER TABLE orderitemcalcs ADD CONSTRAINT fk_orderitemcalcs_orderitem FOREIGN KEY (idorderitem) REFERENCES orderitems;
ALTER TABLE orderitemcalcs ADD CONSTRAINT fk_orderitemcalcs_modelparam FOREIGN KEY (idmodelparam) REFERENCES modelparams;

ALTER TABLE orderitemmodelvalues ADD CONSTRAINT fk_orderitemmodelvalues_orderitem FOREIGN KEY (idorderitem) REFERENCES orderitems;
ALTER TABLE orderitemmodelvalues ADD CONSTRAINT fk_orderitemmodelvalues_modelparamitem FOREIGN KEY (idmodelparamitem) REFERENCES modelparamitems;
ALTER TABLE orderitemmodelvalues ADD CONSTRAINT fk_orderitemmodelvalues_measure FOREIGN KEY (idmeasure) REFERENCES measures;


ALTER TABLE orderitems ADD CONSTRAINT fk_orderitems_order FOREIGN KEY (idorder) REFERENCES orders;
ALTER TABLE orderitems ADD CONSTRAINT fk_orderitems_model FOREIGN KEY (idmodel) REFERENCES models;
--ALTER TABLE orderitems ADD CONSTRAINT fk_orderitems_modeltype FOREIGN KEY (idmodeltype) REFERENCES modeltypes;
ALTER TABLE orders ADD CONSTRAINT fk_orders_peoples FOREIGN KEY (idworker) REFERENCES peoples;
ALTER TABLE orders ADD CONSTRAINT fk_orders_department FOREIGN KEY (iddepartment) REFERENCES departments;
ALTER TABLE orders ADD CONSTRAINT fk_orders_contragents FOREIGN KEY (idcontragent) REFERENCES contragents;
ALTER TABLE orders ADD CONSTRAINT fk_orders_states FOREIGN KEY (idstate) REFERENCES states;
ALTER TABLE people2contragent ADD CONSTRAINT fk_people2contragent_people FOREIGN KEY (idpeople) REFERENCES peoples;
ALTER TABLE people2contragent ADD CONSTRAINT fk_people2contragent_contragent FOREIGN KEY (idcontragent) REFERENCES contragents;
ALTER TABLE people2department ADD CONSTRAINT fk_people2department_people FOREIGN KEY (idpeople) REFERENCES peoples;
ALTER TABLE people2department ADD CONSTRAINT fk_people2department_department FOREIGN KEY (iddepartment) REFERENCES departments;
ALTER TABLE people2right ADD CONSTRAINT fk_people2right_people FOREIGN KEY (idpeople) REFERENCES peoples;
ALTER TABLE people2right ADD CONSTRAINT fk_people2right_right FOREIGN KEY (idright) REFERENCES rights;
ALTER TABLE people2role ADD CONSTRAINT fk_people2role_people FOREIGN KEY (idpeople) REFERENCES peoples;
ALTER TABLE people2role ADD CONSTRAINT fk_people2role_role FOREIGN KEY (idrole) REFERENCES roles;
ALTER TABLE peoples ADD CONSTRAINT fk_peoples_creator FOREIGN KEY (idcreator) REFERENCES peoples;
ALTER TABLE peoples ADD CONSTRAINT fk_peoples_state FOREIGN KEY (idstate) REFERENCES states;
ALTER TABLE prepacks ADD CONSTRAINT fk_prepacks_model FOREIGN KEY (idmodel) REFERENCES models;
ALTER TABLE pricesbuy ADD CONSTRAINT fk_pricesbuy_material FOREIGN KEY (idmaterial) REFERENCES materials;
ALTER TABLE pricesbuy ADD CONSTRAINT fk_pricesbuy_measure FOREIGN KEY (idmeasure) REFERENCES measures;
ALTER TABLE pricesbuy ADD CONSTRAINT fk_pricesbuy_currency FOREIGN KEY (idcurrency) REFERENCES currencies;
ALTER TABLE states ADD CONSTRAINT fk_states_group FOREIGN KEY (idgroup) REFERENCES groups;
