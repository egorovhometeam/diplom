\c crmdb

TRUNCATE departments RESTART IDENTITY CASCADE;
TRUNCATE roles RESTART IDENTITY CASCADE;
TRUNCATE users RESTART IDENTITY CASCADE;

INSERT INTO departments (id,name) VALUES 
(1,'dev')
,(2,'Администрация')
,(3,'Бухгалтерия')
,(4,'Склад')
,(5,'Офис')
;

INSERT INTO roles (id,name,access) VALUES
(1,'dev',1)
,(2,'owner',2)
,(3,'manager',3)
,(4,'emploee',4) 
;

INSERT INTO users (id,id_department,id_role,login,full_name,pwd,birthDate,beginDate) VALUES 
(1,1,1,'dev','Егоров Антон','supervisor','1994-09-03',now())
,(2,2,2,'owner','Иванов Сергей Игоревич','owner','1969-12-08',now()) -- директор
,(3,3,3,'oleg','Олег','oleg','1970-09-21',now())               -- 1ый менеджер
,(4,4,4,'ivan','Иван','ivan','1970-03-15',now())              --кладовщик
,(5,5,3,'masha','Мария','masha','1974-03-22',now())     -- 2ой менеджер
;

