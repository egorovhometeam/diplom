# README #

This README would normally document whatever steps are necessary to get your application up and running.

installing sequence https://docs.djangoproject.com/en/1.11/intro/contributing/

For project need to use PostgreSQL DB: crmdb with user crmadmin and password crmadminpwd
Create crmdb database:
1. First create user: createuser -U postgres -P --interactive crmadmin (and set password to crmadminpwd)
2. Second create database. Better do it in postgres session in database console:
psql -U postgres
# CREATE DATABASE crmdb WITH OWNER = crmadmin TEMPLATE = template0 LC_COLLATE = 'Russian, Russia' LC_CTYPE = 'Russian, Russia';

Check configuration:
psql -U crmadmin
> \dt
And see empty database

sudo apt-get install libmemcached-dev zlib1g-dev
pip install -r requirements/py3.txt

in virtenv 
	pip install virtualenv                (if needed)
	pip install django                    (better to use this way because Django 2.x not working with our project, needed 1.11.3 release)
    pip install django-google-charts
    pip install django-qsstats-magic
    pip install python-dateutil
    pip install psycopg2       
	
Next clone diplom repository:
cd ~/anton/
git clone git@bitbucket.org:egorovhometeam/diplom.git
cd diplom/DB
psql -U crmadmin crmdb < populate_db03072017.sql

Create virtual environment:
cd HOME_FOLDER
virtualenv .virtualenvs/djangodev (Windows version command)
cd .virtualenvs/djangodev/Scripts
activate
cd DIPLOM_FOLDER
cd project
python manage.py migrate
python manage.py runserver


### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact