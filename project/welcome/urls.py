from django.conf.urls import url,include
from . import views

app_name = 'welcome'
urlpatterns = [
    # url(r'^clientDetail/(?P<id>\w{0,50})/$', views.client_detail, name='clientDetail'),
    url(r'^clientDetail/', views.client_detail, name='clientDetail'),
    url(r'^$', views.index, name='index'),
    url(r'^post/(?P<pk>[0-9]+)/$', views.post_detail, name='post_detail'),
    url(r'^post/new/$', views.post_new, name='post_new'),
    
    url(r'^registration/', views.index, name='reg'),
    url(r'^test/$', views.test, name='test'),

]