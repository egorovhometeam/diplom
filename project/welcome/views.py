# -*- coding: utf-8 -*-
from __future__ import unicode_literals


from django.shortcuts import get_object_or_404, render
from django.http import JsonResponse
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.utils import timezone

from .models import Post
from crm.models import Client, Visit
from .forms import PostForm
from django.core import serializers

# def index(request):
# 	posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
# 	return render(request, 'project/base.html',{'posts': posts})

def index(request):
	client = Client.objects.all()
	visit = Visit.objects.all()
	content = {
		'Clients' : client,
		'Visits' : visit
	}
	return render(request, 'welcome/welcome.html', content)

def client_detail(request):
	wanted = request.POST.get('cl_id')
	visit = Visit.objects.filter(id_client=wanted).values('id_client__login','date','id_event__name')
	return JsonResponse({'listVal': list(visit)})
	# visit = Visit.objects.filter(id_client=wanted)
	# data = serializers.serialize('json', visit)
	# return JsonResponse(data, safe=False)

def test(request):
    return render(request, 'welcome/test.html')

def post_detail(request, pk):
        post = get_object_or_404(Post, pk=pk)
        return render(request, 'welcome/post_detail.html', {'post': post})

def post_new(request):
        form = PostForm()
        return render(request, 'welcome/post_edit.html', {'form': form})
        