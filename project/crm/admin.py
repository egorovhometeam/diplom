# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin

from .models import Client,Role,Event,Visit

class VisitAdmin(admin.ModelAdmin):
    fields = ['id_client', 'id_event','date','id_manager']
    list_display = ('id_client', 'id_event','date','id_manager')


admin.site.register(Client)
admin.site.register(Role)
admin.site.register(Event)
admin.site.register(Visit,VisitAdmin)