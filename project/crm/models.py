from __future__ import unicode_literals

from django.db import models

    
class Role(models.Model):
    name = models.CharField(max_length=20, blank=True, null=True)
    access = models.IntegerField(blank=True, null=True)
    def __unicode__(self):
    	return self.name


class Client(models.Model):
    id_role = models.ForeignKey(Role, on_delete=models.CASCADE)
    login = models.CharField(max_length=20, blank=True, null=True)
    full_name = models.CharField(max_length=30, blank=True, null=True)
    pwd = models.CharField(max_length=10, blank=True, null=True)
    begindate = models.DateField(blank=True, null=True)
    birthdate = models.DateField(blank=True, null=True)
    phone = models.CharField(max_length=15, blank=True, null=True)
    email = models.EmailField(blank=True, null=True)
    site = models.CharField(max_length=50, blank=True, null=True)
    SOCIAL_STATUS = (
        (1,'school'),
        (2,'student'),
        (3,'worker'),
        (0,'dependent')
    )
    SEX = (
        (1,'male'),
        (0,'female')
    )
    social_status = models.CharField(choices=SOCIAL_STATUS, max_length=2,default=0)
    sex = models.CharField(choices=SEX,  max_length=2,default=1)
    def __unicode__(self):
        return self.login

class Event(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    fee = models.FloatField(blank=True, null=True)
    notes = models.TextField(blank=True, null=True)

    def __unicode__(self):
        return self.name

class Visit (models.Model):
    id_client = models.ForeignKey(Client, on_delete=models.CASCADE)
    id_event  = models.ForeignKey(Event, on_delete=models.CASCADE)
    id_manager = models.ForeignKey('auth.User', default=1)
    date = models.DateField(blank=True, null=True)
    note = models.CharField(max_length=50, blank=True, null=True)
    def __unicode__(self):
        return str(self.pk)