# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .forms import VisitForm, ClientForm
from django.shortcuts import get_object_or_404, render
from django.utils import timezone

from django.http import HttpResponse, HttpResponseRedirect
from django.http import JsonResponse
from django.core.urlresolvers import reverse
from django.views import generic
from qsstats import QuerySetStats

from django.core import serializers
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

from .models import Client,Visit,Event
from django.db import connection

# Create your views here.
#-*- coding:utf-8 -*-

@login_required(login_url='/registration/login/')
def index(request):
    clForm = ClientForm()
    form = VisitForm()
    form.fields['id_event'].label = "Событие"
    form.fields['note'].label = "Примечания"
    client = Client.objects.all()
    if request.method == "POST":
        clForm = ClientForm(request.POST)
        form = VisitForm(request.POST)
        form.fields['id_event'].label = "Событие"
        form.fields['note'].label = "Примечания"
        if form.is_valid():
            visit = form.save(commit=False)
            visit.date = timezone.now()
            if request.user.is_authenticated():
                me = request.user
            visit.id_manager = me
            # v11 = request.POST.get(u'id_client')
            v1 = Client.objects.get(login=request.POST.get(u'id_client'))
            visit.id_client = v1
            v3 = visit.id_event
            if visit.id_client.pk ==2:
                f1 = False
            else:
                f1 = True
            v2 = visit.date
            if Visit.objects.filter(id_client=v1, date=v2, id_event=v3).exists() and f1:
                print (v1, v2, v3, f1,'was')
                form.add_error(None, 'Такой посетитель уже учтен')
                return render(request, 'crm/manager.html',{'form': form,'Clients': client})
            else:
                print (v1, v2, v3, f1,'was not')
                visit.save()
                form.add_error(None, 'Посетитель учтен')
                return render(request, 'crm/manager.html',{'form': form,'Clients': client})
        else:
            form = VisitForm()
        if clForm.is_valid():
            client = clForm.save()
            return render(request, 'crm/manager.html',{'form': form,'Clients': client})
    return render(request, 'crm/manager.html',{'form': form,'Clients': client})


@login_required(login_url='/registration/login/')
def charts(request):
    data = ClientForm(request.POST)
    return render(request, 'crm/charts.html',{'clientForm': data})

def charts_get(request):
    date1=request.POST.get(u'id1')
    date2=request.POST.get(u'id2')
    def dictfetchall(cursor):
        columns = [col[0] for col in cursor.description]
        return [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]

    with connection.cursor() as c:
        c.execute('select date, count(id) as visits from crm_visit where date between \''+date1+'\' and \''+date2+'\' group by date order by date')
        return JsonResponse(dictfetchall(c), safe=False)
    return render(request, 'crm/charts.html')


def client_record(request):
    wanted = request.POST.get('cl_id')
    visit = Visit.objects.filter(id_client=wanted).values('id_client__login','date','id_event__name')
    # visit = Visit.objects.filter(id_client=wanted)
    # data = serializers.serialize('json', visit, fields('id_client','date','id_event'))
    # return JsonResponse(visit, safe=False)
    return JsonResponse({'listVal': list(visit)})

def client_auto(request):
    term = request.POST.get('keyword')
    listClients = Client.objects.filter(login__icontains=term).values('login','phone','birthdate','id')
    return JsonResponse({'listVal':list(listClients)})

