from django import forms
from .models import Client,Visit,Event,Role

class VisitForm(forms.ModelForm):
    class Meta:
        model = Visit
        fields = ('id_event','note',)

class ClientForm(forms.ModelForm):
    class Meta:
        model = Client
        fields = ('id_role','login','birthdate','phone','social_status','sex',)
