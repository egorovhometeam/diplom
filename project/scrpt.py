import random, datetime
import numpy as np
from crm.models import Client, Visit, Role, Event
from django.contrib.auth.models import User

from datetime import datetime, date, time, timedelta

from django.db import connection

# generating random date in range
# def random_date(start, end):
#     delta = end - start
#     int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
#     random_second = randrange(int_delta)
#     return start + timedelta(seconds=random_second)
# d1 = datetime(2005, 7, 14)
# d2 = datetime.now()
# print random_date(d1, d2)

# random.triangular(0,9,1) - 0,9 limits; 1 - center
# a = [0,0,0,0,0,0,0,0,0,0]
# for x in xrange(1,100):
# 	i = int(random.triangular(0,9,1))
# 	a[i] += 1 
# 	x += 1 
# 	pass
# print a

# c = Visit(
# 	id_client = Client.objects.get(id=random.randint(1,200)),
# 	id_event  = Event.objects.get(id=int(random.triangular(1,5,1))),
# 	id_manager = User.objects.get(id=2),
# 	date = datetime(2017,10,2),
# 	note = 'ManualAddedVisit'
# )
# c.save()

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

with connection.cursor() as c:
    c.execute('select date as id, count(id) as my_count from crm_visit group by date')
    dictfetchall(c)

# for p in Visit.objects.raw('select date as id, count(id) as my_count from crm_visit group by date'):
#     print(p)
