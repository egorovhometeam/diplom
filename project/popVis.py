from crm.models import Client, Visit, Role, Event
from django.contrib.auth.models import User
import random
from random import randrange
from datetime import datetime, date, time, timedelta

# foo = ['a', 'b', 'c', 'd', 'e']
# print(random.choice(foo))
# print('a')

# class Visit (models.Model):
#     id_client = models.ForeignKey(Client, on_delete=models.CASCADE)
#     id_event  = models.ForeignKey(Event, on_delete=models.CASCADE)
#     id_manager = models.ForeignKey('auth.User', default=1)
#     date = models.DateField(blank=True, null=True)
#     note = models.CharField(max_length=50, blank=True, null=True)
#     def __unicode__(self):
#         return str(self.pk)

# u = Client.objects.get(login=v11)
# r = Role.objects.get()
# e = Event.objects.get()
# User.objects.get()
def daterange(start_date, end_date):
    for n in range(int ((end_date - start_date).days)):
        yield start_date + timedelta(n)

start_date = date(2017, 1, 1)
end_date = date(2017, 1, 30)


Visit.objects.filter(note__icontains='Autovisit').delete()

for single_date in daterange(start_date, end_date):
	print single_date.strftime("%Y-%m-%d")
	a = random.randint(10,76)
	for x in xrange(1,a):
		try:
			cl = Client.objects.get(id=random.randint(2,198))
		except Client.DoesNotExist:
			cl = Client.objects.get(id=4)
		c = Visit(
			id_client = cl,
			id_event  = Event.objects.get(id=int(random.triangular(1,5,1))),
			id_manager = User.objects.get(id=2),
			date = single_date,
			note = 'Autovisit '+str(x)
		)
		c.save()
		pass